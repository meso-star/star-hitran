/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L

#include "shtr.h"

#include <rsys/clock_time.h>
#include <rsys/mem_allocator.h>
#include <rsys/math.h>

#include <math.h>
#include <string.h>

static void
print_lines
  (FILE* fp,
   const struct shtr_line* lines,
   const size_t nlines)
{
  size_t i;

  CHK(fp && (!nlines || lines));
  FOR_EACH(i, 0, nlines) {
    fprintf(fp,
      "%2d%1d%12.6f%10.3e 0.000E-00.%04d%5.3f%10.4f%4.2f%8.6f"
      /* Dummy remaining data */
      "          0 0 0" /* Global upper quanta */
      "          0 0 0" /* Global upper quanta */
      "  5  5  0      " /* Local upper quanta */
      "  5  5  1      " /* Local lower quanta */
      "562220" /* Error indices */
      "5041 7833348" /* References */
      " " /* Line mixing flag */
      "   66.0" /* g' */
      "   66.0" /* g'' */
      "\n",
      lines[i].molecule_id,
      lines[i].isotope_id_local == 9 ? 0 : lines[i].isotope_id_local+1,
      lines[i].wavenumber,
      lines[i].intensity,
      (int)(lines[i].gamma_air*10000+0.5/*round*/),
      lines[i].gamma_self,
      lines[i].lower_state_energy,
      lines[i].n_air,
      lines[i].delta_air);
  }
}

static void
test_line_eq(void)
{
  const struct shtr_line l0 = {
    0.000134, 2.672E-38, 0.0533, 0.410, 608.4727, 0.79, 0.000060, 1, 4
  };
  struct shtr_line l1 = l0;

  CHK(shtr_line_eq(&l0, &l1));

  l1.wavenumber = nextafter(l1.wavenumber, INF);
  CHK(!shtr_line_eq(&l0, &l1));
  l1.wavenumber = l0.wavenumber;
  l1.intensity = nextafter(l1.intensity, INF);
  CHK(!shtr_line_eq(&l0, &l1));
  l1.intensity = l0.intensity;
  l1.gamma_air = nextafter(l1.gamma_air, INF);
  CHK(!shtr_line_eq(&l0, &l1));
  l1.gamma_air = l0.gamma_air;
  l1.gamma_self = nextafter(l1.gamma_self, INF);
  CHK(!shtr_line_eq(&l0, &l1));
  l1.gamma_self = l0.gamma_self;
  l1.lower_state_energy = nextafter(l1.lower_state_energy, INF);
  CHK(!shtr_line_eq(&l0, &l1));
  l1.lower_state_energy = l0.lower_state_energy;
  l1.n_air = nextafter(l1.n_air, INF);
  CHK(!shtr_line_eq(&l0, &l1));
  l1.n_air = l0.n_air;
  l1.delta_air = nextafter(l1.delta_air, INF);
  CHK(!shtr_line_eq(&l0, &l1));
  l1.delta_air = l0.delta_air;
  l1.molecule_id += 1;
  CHK(!shtr_line_eq(&l0, &l1));
  l1.molecule_id = l0.molecule_id;
  l1.isotope_id_local += 1;
  CHK(!shtr_line_eq(&l0, &l1));
  l1.isotope_id_local = l0.isotope_id_local;

  CHK(shtr_line_eq(&l0, &l1));
}

static void
test_load(struct shtr* shtr)
{
  const struct shtr_line l[] = {
    {0.000134, 2.672E-38, 0.0533, 0.410, 608.4727, 0.79, 0.000060, 1, 4},
    {0.000379, 1.055E-39, 0.0418, 0.329,1747.9686, 0.79, 0.000110, 1, 5},
    {0.000448, 5.560E-38, 0.0490, 0.364,1093.0269, 0.79, 0.000060, 1, 4},
    {0.000686, 1.633E-36, 0.0578, 0.394, 701.1162, 0.79, 0.000180, 1, 4},
    {0.000726, 6.613E-33, 0.0695, 0.428, 402.3295, 0.79, 0.000240, 1, 3}
  };
  const size_t nlines = sizeof(l) / sizeof(struct shtr_line);

  struct shtr_line_list* list = NULL;
  const struct shtr_line* lines = NULL;
  const char* filename = "test_lines.txt";
  FILE* fp = NULL;
  size_t i, n;

  CHK(fp = fopen(filename, "w+"));
  print_lines(fp, l, nlines);
  rewind(fp);

  CHK(shtr_line_list_load_stream(NULL, fp, NULL, &list) == RES_BAD_ARG);
  CHK(shtr_line_list_load_stream(shtr, NULL, NULL, &list) == RES_BAD_ARG);
  CHK(shtr_line_list_load_stream(shtr, fp, NULL, NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_load_stream(shtr, fp, NULL, &list) == RES_OK);

  CHK(shtr_line_list_get_size(NULL, &n) == RES_BAD_ARG);
  CHK(shtr_line_list_get_size(list, NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_get_size(list, &n) == RES_OK);
  CHK(n == nlines);

  CHK(shtr_line_list_get(NULL, &lines) == RES_BAD_ARG);
  CHK(shtr_line_list_get(list, NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_get(list, &lines) == RES_OK);
  FOR_EACH(i, 0, n) CHK(shtr_line_eq(lines+i, l+i));

  CHK(shtr_line_list_ref_get(NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_ref_get(list) == RES_OK);
  CHK(shtr_line_list_ref_put(NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_ref_put(list) == RES_OK);
  CHK(shtr_line_list_ref_put(list) == RES_OK);

  CHK(fclose(fp) == 0);

  CHK(shtr_line_list_load(NULL, filename, &list) == RES_BAD_ARG);
  CHK(shtr_line_list_load(shtr, NULL, &list) == RES_BAD_ARG);
  CHK(shtr_line_list_load(shtr, filename, NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_load(shtr, filename, &list) == RES_OK);

  CHK(shtr_line_list_get_size(list, &n) == RES_OK);
  CHK(n == nlines);

  CHK(shtr_line_list_get(list, &lines) == RES_OK);
  FOR_EACH(i, 0, n) CHK(shtr_line_eq(lines+i, l+i));

  CHK(shtr_line_list_ref_put(list) == RES_OK);
}

static void
test_line
  (struct shtr* shtr, const struct shtr_line* ln, const res_T res)
{
  struct shtr_line_list* list = NULL;
  FILE* fp = NULL;

  CHK(fp = tmpfile());
  print_lines(fp, ln, 1);
  rewind(fp);
  CHK(shtr_line_list_load_stream(shtr, fp, NULL, &list) == res);
  CHK(fclose(fp) == 0);

  if(res == RES_OK) CHK(shtr_line_list_ref_put(list) == RES_OK);
}

static void
test_load_failures(struct shtr* shtr)
{
  const struct shtr_line ln_ref = {
    0.000134, 2.672E-38, 0.0533, 0.410, 608.4727, 0.79, 0.000060, 1, 4,
  };
  struct shtr_line ln;
  struct shtr_line_list* list = NULL;
  FILE* fp = NULL;

  /* Check that the reference line is valid */
  test_line(shtr, &ln_ref, RES_OK);

  /* Invalid wavenumber */
  ln = ln_ref; ln.wavenumber = 0;
  test_line(shtr, &ln, RES_BAD_ARG);

  /* Invalid intensity */
  ln = ln_ref; ln.intensity = 0;
  test_line(shtr, &ln, RES_BAD_ARG);

  /* Invalid gamma air */
  ln = ln_ref; ln.gamma_air = -1;
  test_line(shtr, &ln, RES_BAD_ARG);

  /* Invalid gamma self */
  ln = ln_ref; ln.gamma_self = -1;
  test_line(shtr, &ln, RES_BAD_ARG);

  /* Unavailable lower state energy */
  ln = ln_ref; ln.lower_state_energy = -1;
  test_line(shtr, &ln, RES_OK);

  /* Invalid lower state energy */
  ln = ln_ref; ln.lower_state_energy = -2;
  test_line(shtr, &ln, RES_BAD_ARG);

  /* Invalid molecule id */
  ln = ln_ref; ln.molecule_id = -1;
  test_line(shtr, &ln, RES_BAD_ARG);

  /* Bad file formatting */
  ln = ln_ref; ln.molecule_id = 100;
  test_line(shtr, &ln, RES_BAD_ARG);
  ln = ln_ref; ln.isotope_id_local = 10;
  test_line(shtr, &ln, RES_BAD_ARG);

  /* Lines are not correctly sorted */
  CHK(fp = tmpfile());
  ln = ln_ref;
  print_lines(fp, &ln, 1);
  ln.wavenumber -= 1e-4;
  print_lines(fp, &ln, 1);
  rewind(fp);
  CHK(shtr_line_list_load_stream(shtr, fp, NULL, &list) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);
}

static void
check_line_list_equality
  (const struct shtr_line_list* list1,
   const struct shtr_line_list* list2)
{
  const struct shtr_line* lines1 = NULL;
  const struct shtr_line* lines2 = NULL;
  size_t n1, n2;
  size_t iline, nlines;
  CHK(list1 && list2);

  CHK(shtr_line_list_get_size(list1, &n1) == RES_OK);
  CHK(shtr_line_list_get_size(list2, &n2) == RES_OK);
  CHK(n1 == n2);
  nlines = n1;

  CHK(shtr_line_list_get(list1, &lines1) == RES_OK);
  CHK(shtr_line_list_get(list2, &lines2) == RES_OK);
  FOR_EACH(iline, 0, nlines) {
    CHK(shtr_line_eq(lines1+iline, lines2+iline));
  }
}

static void
test_serialization(struct shtr* shtr)
{
  const struct shtr_line l[] = {
    {0.000134, 2.672E-38, 0.0533, 0.410, 608.4727, 0.79, 0.000060, 1, 4},
    {0.000379, 1.055E-39, 0.0418, 0.329,1747.9686, 0.79, 0.000110, 1, 5},
    {0.000448, 5.560E-38, 0.0490, 0.364,1093.0269, 0.79, 0.000060, 1, 4},
    {0.000686, 1.633E-36, 0.0578, 0.394, 701.1162, 0.79, 0.000180, 1, 4},
    {0.000726, 6.613E-33, 0.0695, 0.428, 402.3295, 0.79, 0.000240, 1, 3}
  };
  const size_t nlines = sizeof(l) / sizeof(struct shtr_line);

  struct shtr_line_list* list1 = NULL;
  struct shtr_line_list* list2 = NULL;
  FILE* fp = NULL;

  CHK(fp = tmpfile());
  print_lines(fp, l, nlines);
  rewind(fp);

  CHK(shtr_line_list_load_stream(shtr, fp, NULL, &list1) == RES_OK);
  fclose(fp);

  CHK(fp = tmpfile());
  CHK(shtr_line_list_write(NULL, fp) == RES_BAD_ARG);
  CHK(shtr_line_list_write(list1, NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_write(list1, fp) == RES_OK);
  rewind(fp);

  CHK(shtr_line_list_create_from_stream(NULL, fp, &list2) == RES_BAD_ARG);
  CHK(shtr_line_list_create_from_stream(shtr, NULL, &list2) == RES_BAD_ARG);
  CHK(shtr_line_list_create_from_stream(shtr, fp, NULL) == RES_BAD_ARG);
  CHK(shtr_line_list_create_from_stream(shtr, fp, &list2) == RES_OK);
  fclose(fp);

  check_line_list_equality(list1, list2);

  CHK(shtr_line_list_ref_put(list1) == RES_OK);
  CHK(shtr_line_list_ref_put(list2) == RES_OK);
}

static void
check_view
  (const struct shtr_line_list* list,
   const struct shtr_line_view* view,
   const struct shtr_line_view_create_args* args)
{
  const struct shtr_line* lines = NULL;
  size_t i, n;
  size_t nlines_selected = 0;
  CHK(list && view && args);

  CHK(shtr_line_list_get_size(list, &n) == RES_OK);
  CHK(shtr_line_list_get(list, &lines) == RES_OK);
  FOR_EACH(i, 0, n) {
    const struct shtr_line* line = NULL;
    double nu = 0;
    size_t imolecule;

    /* Discard lines that does not belongs to a listed molecule */
    FOR_EACH(imolecule, 0, args->nmolecules) {
      if(lines[i].molecule_id == args->molecules[imolecule].id) break;
    }
    if(imolecule >= args->nmolecules)
      continue; /* Skip the molecule */

    /* Compute the line center for the given pressure */
    nu = lines[i].wavenumber + lines[i].delta_air * args->pressure;

    /* Discard lines that are out of ranges */
    if(nu + args->molecules[imolecule].cutoff < args->wavenumber_range[0])
      continue;
    if(nu - args->molecules[imolecule].cutoff > args->wavenumber_range[1])
      continue;

    /* nisotope == 0 <=> All isotopes must be selected */
    if(args->molecules[imolecule].nisotopes) {
      size_t iisotope;
      FOR_EACH(iisotope, 0, args->molecules[imolecule].nisotopes) {
        if(lines[i].isotope_id_local
        == args->molecules[imolecule].isotope_ids_local[iisotope])
          break;
      }
      if(iisotope >= args->molecules[imolecule].nisotopes)
        continue; /* Skip the isotope */
    }

    CHK(shtr_line_view_get_line(view, nlines_selected, &line) == RES_OK);
    CHK(shtr_line_eq(line, lines+i));
    nlines_selected += 1;
  }

  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == nlines_selected);
}

static void
test_view(struct shtr* shtr)
{
  const struct shtr_line l[] = {
    {0.00156, 2.685e-36, 0.0695, 0.428,  399.7263, 0.79, 0.000240, 1, 5},
    {0.03406, 5.898e-37, 0.0883, 0.410, 1711.6192, 0.79, 0.000000, 1, 5},
    {0.03445, 5.263e-33, 0.0504, 0.329, 1570.0616, 0.79, 0.001940, 1, 3},
    {0.03628, 9.296e-29, 0.0704, 0.095,  522.5576, 0.81, 0.000000, 3, 0},
    {0.03671, 2.555e-37, 0.0389, 0.304, 2337.5190, 0.79, 0.002660, 1, 4},
    {0.08403, 8.190e-33, 0.0753, 0.394,  568.0017, 0.79, 0.002180, 1, 4},
    {0.08653, 5.376e-29, 0.0691, 0.083, 1061.6864, 0.76, 0.000000, 3, 0},
    {0.09642, 6.675e-37, 0.0570, 0.351, 2516.3150, 0.79, 0.000000, 1, 4},
    {0.16772, 2.456e-28, 0.0823, 0.105,  751.2922, 0.77, 0.000000, 3, 0},
    {0.18582, 5.338e-32, 0.0925, 0.428, 1717.3356, 0.79, 0.004100, 1, 3},
    {0.19368, 2.553e-32, 0.0901, 0.428,  293.8010, 0.79, 0.001260, 1, 5},
    {0.19688, 1.447e-31, 0.0901, 0.428,  292.3093, 0.79, 0.001260, 1, 4},
    {0.19757, 6.063e-29, 0.0681, 0.078, 1401.6146, 0.76, 0.000000, 3, 0},
    {0.21281, 8.238e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21282, 5.999e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21283, 7.737e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21283, 6.394e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21284, 7.260e-31, 0.0780, 0.103,  127.4732, 0.78, 0.000000, 3, 4},
    {0.21284, 6.813e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21728, 5.928e-29, 0.0823, 0.105, 1153.4099, 0.77, 0.000000, 3, 0},
    {0.25818, 3.110e-32, 0.0539, 0.311, 1958.1248, 0.79, 0.008600, 1, 3},
    {0.26618, 1.468e-32, 0.0801, 0.378, 2118.9452, 0.79, 0.003100, 1, 3},
    {0.27091, 1.362e-35, 0.0539, 0.311, 1949.2032, 0.79, 0.008600, 1, 5},
    {0.28910, 2.058e-30, 0.0878, 0.106,    7.8611, 0.76, 0.000000, 3, 3},
    {0.29412, 8.666e-33, 0.0795, 0.378,  679.8760, 0.79, 0.004170, 1, 5},
    {0.29477, 1.457e-30, 0.0660, 0.340, 1238.7943, 0.79, 0.007260, 1, 3},
    {0.29673, 1.609e-30, 0.0773, 0.103,  138.7916, 0.78, 0.000000, 3, 3},
    {0.29673, 1.195e-30, 0.0773, 0.103,  138.7916, 0.78, 0.000000, 3, 3},
    {0.29676, 1.268e-30, 0.0773, 0.103,  138.7916, 0.78, 0.000000, 3, 3},
    {1.46280, 7.205e-26, 0.0696, 0.090,  702.3167, 0.82, 0.000000, 3, 0},
    {1.46899, 7.847e-28, 0.0704, 0.095, 1557.6290, 0.81, 0.000000, 3, 0},
    {1.47237, 1.700e-34, 0.0633, 0.297, 2389.2994, 0.79, 0.013980, 1, 4},
    {1.47273, 1.131e-32, 0.0903, 0.123,    0.7753, 0.69, 0.000814, 2, 2},
    {1.47863, 5.318e-33, 0.0925, 0.428, 2877.6872, 0.79, 0.006200, 1, 3},
    {1.48860, 1.058e-27, 0.0760, 0.102,  163.7760, 0.78, 0.000000, 3, 1},
    {1.49043, 4.787e-31, 0.0773, 0.103,  139.7879, 0.78, 0.000000, 3, 4},
    {1.49044, 3.139e-29, 0.0773, 0.103,  139.7879, 0.78, 0.000000, 3, 4},
    {1.49044, 3.332e-29, 0.0773, 0.103,  139.7880, 0.78, 0.000000, 3, 4},
    {1.49045, 2.785e-29, 0.0773, 0.103,  139.7880, 0.78, 0.000000, 3, 4},
    {1.49045, 2.625e-29, 0.0773, 0.103,  139.7880, 0.78, 0.000000, 3, 4},
    {1.49057, 5.941e-27, 0.0909, 0.106,  703.4398, 0.76, 0.000000, 3, 0},
    {1.49110, 1.537e-29, 0.0714, 0.098,  381.8992, 0.80, 0.000000, 3, 3},
    {1.49111, 1.828e-29, 0.0714, 0.098,  381.8992, 0.80, 0.000000, 3, 3},
    {1.49111, 1.648e-29, 0.0714, 0.098,  381.8992, 0.80, 0.000000, 3, 3},
    {1.49111, 1.766e-29, 0.0714, 0.098,  381.8991, 0.80, 0.000000, 3, 3},
    {1.49111, 1.706e-29, 0.0714, 0.098,  381.8991, 0.80, 0.000000, 3, 3},
    {1.49674, 1.208e-25, 0.0719, 0.102,  190.2125, 0.79, 0.000000, 3, 0},
    {1.50216, 4.971e-28, 0.0696, 0.090, 1734.9796, 0.82, 0.000000, 3, 0},
    {1.51178, 3.047e-27, 0.0766, 0.103, 1195.5580, 0.78, 0.000000, 3, 0},
    {1.51399, 1.986e-27, 0.0765, 0.104, 1129.0675, 0.77, 0.000000, 3, 0},
    {1.51442, 2.601e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51443, 3.981e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51443, 6.637e-35, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51444, 3.902e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51445, 1.394e-33, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51446, 7.166e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51446, 2.787e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51446, 5.096e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51448, 1.672e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51597, 4.443e-30, 0.0704, 0.097,  463.6953, 0.81, 0.000000, 3, 3},
    {1.51600, 5.172e-30, 0.0704, 0.097,  463.6952, 0.81, 0.000000, 3, 3},
    {1.51603, 4.580e-30, 0.0704, 0.097,  463.6952, 0.81, 0.000000, 3, 3},
    {1.51605, 5.019e-30, 0.0704, 0.097,  463.6951, 0.81, 0.000000, 3, 3}
  };
  const size_t nlines = sizeof(l) / sizeof(struct shtr_line);

  const double cutoff = 0.01;

  struct shtr_line_view_create_args args = SHTR_LINE_VIEW_CREATE_ARGS_NULL;
  struct shtr_line_list* list = NULL;
  struct shtr_line_view* view = NULL;
  const struct shtr_line* line = NULL;
  const char* filename = "test_lines.txt";
  FILE* fp = NULL;
  size_t i, n;

  CHK(fp = fopen(filename, "w+"));
  print_lines(fp, l, nlines);
  rewind(fp);

  CHK(shtr_line_list_load_stream(shtr, fp, NULL, &list) == RES_OK);

  args.wavenumber_range[0] = 0;
  args.wavenumber_range[1] = INF;
  args.molecules[0].id = 1;
  args.molecules[1].id = 2;
  args.molecules[2].id = 3;
  args.molecules[0].cutoff = cutoff;
  args.molecules[1].cutoff = cutoff;
  args.molecules[2].cutoff = cutoff;
  args.nmolecules = 3;
  args.pressure = 0;
  CHK(shtr_line_view_create(NULL, &args, &view) == RES_BAD_ARG);
  CHK(shtr_line_view_create(list, NULL, &view) == RES_BAD_ARG);
  CHK(shtr_line_view_create(list, &args, NULL) == RES_BAD_ARG);
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);

  CHK(shtr_line_view_get_size(NULL, &n) == RES_BAD_ARG);
  CHK(shtr_line_view_get_size(view, NULL) == RES_BAD_ARG);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == nlines);

  CHK(shtr_line_view_get_line(NULL, 0, &line) == RES_BAD_ARG);
  CHK(shtr_line_view_get_line(view, n, &line) == RES_BAD_ARG);
  CHK(shtr_line_view_get_line(view, 0, NULL) == RES_BAD_ARG);

  FOR_EACH(i, 0, n) {
    CHK(shtr_line_view_get_line(view, i, &line) == RES_OK);
    CHK(shtr_line_eq(line, l+i));
  }

  CHK(shtr_line_view_ref_get(NULL) == RES_BAD_ARG);
  CHK(shtr_line_view_ref_get(view) == RES_OK);
  CHK(shtr_line_view_ref_put(NULL) == RES_BAD_ARG);
  CHK(shtr_line_view_ref_put(view) == RES_OK);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.wavenumber_range[0] = 1;
  args.wavenumber_range[1] = 0;
  CHK(shtr_line_view_create(list, &args, &view) == RES_BAD_ARG);

  args.wavenumber_range[0] = 0;
  args.wavenumber_range[1] = 1;
  args.nmolecules = 0;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 0);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.pressure = -1;
  CHK(shtr_line_view_create(list, &args, &view) == RES_BAD_ARG);
  args.pressure = 0;
  args.molecules[0].cutoff = -1;
  args.nmolecules = 3;
  CHK(shtr_line_view_create(list, &args, &view) == RES_BAD_ARG);
  args.molecules[0].cutoff = cutoff;

  args.wavenumber_range[0] = nextafter(l[nlines-1].wavenumber, INF) + cutoff;
  args.wavenumber_range[1] = INF;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 0);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.wavenumber_range[0] = 0;
  args.wavenumber_range[1] = 1.50;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.wavenumber_range[0] = 1.50;
  args.wavenumber_range[1] = 1.51603;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.wavenumber_range[0] = 0.29477;
  args.wavenumber_range[1] = 0.29477;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 6);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.wavenumber_range[0] = 0.09642;
  args.wavenumber_range[1] = 0.21283;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 14);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.molecules[0].id = 2;
  args.nmolecules = 1;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 0);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.molecules[1].id = 1;
  args.nmolecules = 2;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 4);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.molecules[1].id = 3;
  args.nmolecules = 2;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 10);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.wavenumber_range[0] = 0;
  args.wavenumber_range[1] = INF;
  args.molecules[0].id = 2;
  args.nmolecules = 1;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.molecules[0].id = 1;
  args.molecules[0].cutoff = 1e-6;
  args.molecules[0].nisotopes = 0;
  args.nmolecules = 1;
  args.wavenumber_range[0] = 0.1899;
  args.wavenumber_range[1] = 0.195;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 1);
  CHK(shtr_line_view_ref_put(view) == RES_OK);
  args.pressure = 1;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 2);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  args.wavenumber_range[0] = 0;
  args.wavenumber_range[1] = INF;
  args.molecules[0].id = 3;
  args.molecules[0].cutoff = cutoff;
  args.molecules[0].isotope_ids_local[0] = 4;
  args.molecules[0].isotope_ids_local[1] = 1;
  args.molecules[0].isotope_ids_local[2] = 2;
  args.molecules[0].isotope_ids_local[3] = 0;
  args.molecules[0].nisotopes = 4;
  args.molecules[1].id = 1;
  args.molecules[0].cutoff = cutoff;
  args.molecules[1].isotope_ids_local[0] = 4;
  args.molecules[1].isotope_ids_local[1] = 3;
  args.molecules[1].nisotopes = 2;
  args.nmolecules = 2;
  CHK(shtr_line_view_create(list, &args, &view) == RES_OK);
  CHK(shtr_line_view_get_size(view, &n) == RES_OK);
  CHK(n == 35);
  check_view(list, view, &args);
  CHK(shtr_line_view_ref_put(view) == RES_OK);

  CHK(shtr_line_list_ref_put(list) == RES_OK);
  CHK(fclose(fp) == 0);
}

static void
check_line_view_equality
  (const struct shtr_line_view* view1,
   const struct shtr_line_view* view2)
{
  size_t n1, n2;
  size_t iline, nlines;
  CHK(view1 && view2);

  CHK(shtr_line_view_get_size(view1, &n1) == RES_OK);
  CHK(shtr_line_view_get_size(view2, &n2) == RES_OK);
  CHK(n1 == n2);
  nlines = n1;

  FOR_EACH(iline, 0, nlines) {
    const struct shtr_line* line1 = NULL;
    const struct shtr_line* line2 = NULL;
    CHK(shtr_line_view_get_line(view1, iline, &line1) == RES_OK);
    CHK(shtr_line_view_get_line(view2, iline, &line2) == RES_OK);
    CHK(shtr_line_eq(line1, line2));
  }
}

static void
test_view_serialization(struct shtr* shtr)
{
  const struct shtr_line l[] = {
    {0.00156, 2.685e-36, 0.0695, 0.428,  399.7263, 0.79, 0.000240, 1, 5},
    {0.03406, 5.898e-37, 0.0883, 0.410, 1711.6192, 0.79, 0.000000, 1, 5},
    {0.03445, 5.263e-33, 0.0504, 0.329, 1570.0616, 0.79, 0.001940, 1, 3},
    {0.03628, 9.296e-29, 0.0704, 0.095,  522.5576, 0.81, 0.000000, 3, 0},
    {0.03671, 2.555e-37, 0.0389, 0.304, 2337.5190, 0.79, 0.002660, 1, 4},
    {0.08403, 8.190e-33, 0.0753, 0.394,  568.0017, 0.79, 0.002180, 1, 4},
    {0.08653, 5.376e-29, 0.0691, 0.083, 1061.6864, 0.76, 0.000000, 3, 0},
    {0.09642, 6.675e-37, 0.0570, 0.351, 2516.3150, 0.79, 0.000000, 1, 4},
    {0.16772, 2.456e-28, 0.0823, 0.105,  751.2922, 0.77, 0.000000, 3, 0},
    {0.18582, 5.338e-32, 0.0925, 0.428, 1717.3356, 0.79, 0.004100, 1, 3},
    {0.19368, 2.553e-32, 0.0901, 0.428,  293.8010, 0.79, 0.001260, 1, 5},
    {0.19688, 1.447e-31, 0.0901, 0.428,  292.3093, 0.79, 0.001260, 1, 4},
    {0.19757, 6.063e-29, 0.0681, 0.078, 1401.6146, 0.76, 0.000000, 3, 0},
    {0.21281, 8.238e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21282, 5.999e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21283, 7.737e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21283, 6.394e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21284, 7.260e-31, 0.0780, 0.103,  127.4732, 0.78, 0.000000, 3, 4},
    {0.21284, 6.813e-31, 0.0780, 0.103,  127.4733, 0.78, 0.000000, 3, 4},
    {0.21728, 5.928e-29, 0.0823, 0.105, 1153.4099, 0.77, 0.000000, 3, 0},
    {0.25818, 3.110e-32, 0.0539, 0.311, 1958.1248, 0.79, 0.008600, 1, 3},
    {0.26618, 1.468e-32, 0.0801, 0.378, 2118.9452, 0.79, 0.003100, 1, 3},
    {0.27091, 1.362e-35, 0.0539, 0.311, 1949.2032, 0.79, 0.008600, 1, 5},
    {0.28910, 2.058e-30, 0.0878, 0.106,    7.8611, 0.76, 0.000000, 3, 3},
    {0.29412, 8.666e-33, 0.0795, 0.378,  679.8760, 0.79, 0.004170, 1, 5},
    {0.29477, 1.457e-30, 0.0660, 0.340, 1238.7943, 0.79, 0.007260, 1, 3},
    {0.29673, 1.609e-30, 0.0773, 0.103,  138.7916, 0.78, 0.000000, 3, 3},
    {0.29673, 1.195e-30, 0.0773, 0.103,  138.7916, 0.78, 0.000000, 3, 3},
    {0.29676, 1.268e-30, 0.0773, 0.103,  138.7916, 0.78, 0.000000, 3, 3},
    {1.46280, 7.205e-26, 0.0696, 0.090,  702.3167, 0.82, 0.000000, 3, 0},
    {1.46899, 7.847e-28, 0.0704, 0.095, 1557.6290, 0.81, 0.000000, 3, 0},
    {1.47237, 1.700e-34, 0.0633, 0.297, 2389.2994, 0.79, 0.013980, 1, 4},
    {1.47273, 1.131e-32, 0.0903, 0.123,    0.7753, 0.69, 0.000814, 2, 2},
    {1.47863, 5.318e-33, 0.0925, 0.428, 2877.6872, 0.79, 0.006200, 1, 3},
    {1.48860, 1.058e-27, 0.0760, 0.102,  163.7760, 0.78, 0.000000, 3, 1},
    {1.49043, 4.787e-31, 0.0773, 0.103,  139.7879, 0.78, 0.000000, 3, 4},
    {1.49044, 3.139e-29, 0.0773, 0.103,  139.7879, 0.78, 0.000000, 3, 4},
    {1.49044, 3.332e-29, 0.0773, 0.103,  139.7880, 0.78, 0.000000, 3, 4},
    {1.49045, 2.785e-29, 0.0773, 0.103,  139.7880, 0.78, 0.000000, 3, 4},
    {1.49045, 2.625e-29, 0.0773, 0.103,  139.7880, 0.78, 0.000000, 3, 4},
    {1.49057, 5.941e-27, 0.0909, 0.106,  703.4398, 0.76, 0.000000, 3, 0},
    {1.49110, 1.537e-29, 0.0714, 0.098,  381.8992, 0.80, 0.000000, 3, 3},
    {1.49111, 1.828e-29, 0.0714, 0.098,  381.8992, 0.80, 0.000000, 3, 3},
    {1.49111, 1.648e-29, 0.0714, 0.098,  381.8992, 0.80, 0.000000, 3, 3},
    {1.49111, 1.766e-29, 0.0714, 0.098,  381.8991, 0.80, 0.000000, 3, 3},
    {1.49111, 1.706e-29, 0.0714, 0.098,  381.8991, 0.80, 0.000000, 3, 3},
    {1.49674, 1.208e-25, 0.0719, 0.102,  190.2125, 0.79, 0.000000, 3, 0},
    {1.50216, 4.971e-28, 0.0696, 0.090, 1734.9796, 0.82, 0.000000, 3, 0},
    {1.51178, 3.047e-27, 0.0766, 0.103, 1195.5580, 0.78, 0.000000, 3, 0},
    {1.51399, 1.986e-27, 0.0765, 0.104, 1129.0675, 0.77, 0.000000, 3, 0},
    {1.51442, 2.601e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51443, 3.981e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51443, 6.637e-35, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51444, 3.902e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51445, 1.394e-33, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51446, 7.166e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51446, 2.787e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51446, 5.096e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51448, 1.672e-34, 0.0903, 0.123,    0.7572, 0.69, 0.000814, 2, 3},
    {1.51597, 4.443e-30, 0.0704, 0.097,  463.6953, 0.81, 0.000000, 3, 3},
    {1.51600, 5.172e-30, 0.0704, 0.097,  463.6952, 0.81, 0.000000, 3, 3},
    {1.51603, 4.580e-30, 0.0704, 0.097,  463.6952, 0.81, 0.000000, 3, 3},
    {1.51605, 5.019e-30, 0.0704, 0.097,  463.6951, 0.81, 0.000000, 3, 3}
  };
  const size_t nlines = sizeof(l) / sizeof(struct shtr_line);

  const double cutoff = 25;

  struct shtr_line_view_create_args args = SHTR_LINE_VIEW_CREATE_ARGS_NULL;
  struct shtr_line_list* list = NULL;
  struct shtr_line_view* view1 = NULL;
  struct shtr_line_view* view2 = NULL;
  FILE* fp = NULL;

  CHK(fp = tmpfile());
  print_lines(fp, l, nlines);
  rewind(fp);

  CHK(shtr_line_list_load_stream(shtr, fp, NULL, &list) == RES_OK);
  fclose(fp);

  args.wavenumber_range[0] = 0;
  args.wavenumber_range[1] = INF;
  args.molecules[0].id = 1;
  args.molecules[1].id = 2;
  args.molecules[2].id = 3;
  args.molecules[0].cutoff = cutoff;
  args.molecules[1].cutoff = cutoff;
  args.molecules[2].cutoff = cutoff;
  args.nmolecules = 3;
  args.pressure = 0;
  CHK(shtr_line_view_create(list, &args, &view1) == RES_OK);
  CHK(shtr_line_list_ref_put(list) == RES_OK);

  CHK(fp = tmpfile());
  CHK(shtr_line_view_write(NULL, fp) == RES_BAD_ARG);
  CHK(shtr_line_view_write(view1, NULL) == RES_BAD_ARG);
  CHK(shtr_line_view_write(view1, fp) == RES_OK);
  rewind(fp);

  CHK(shtr_line_view_create_from_stream(NULL, fp, &view2) == RES_BAD_ARG);
  CHK(shtr_line_view_create_from_stream(shtr, NULL, &view2) == RES_BAD_ARG);
  CHK(shtr_line_view_create_from_stream(shtr, fp, NULL) == RES_BAD_ARG);
  CHK(shtr_line_view_create_from_stream(shtr, fp, &view2) == RES_OK);
  fclose(fp);

  check_line_view_equality(view1, view2);

  CHK(shtr_line_view_ref_put(view1) == RES_OK);
  CHK(shtr_line_view_ref_put(view2) == RES_OK);
}

static void
check_line(const struct shtr_line* ln)
{
  /* Check NaN */
  CHK(ln->wavenumber == ln->wavenumber);
  CHK(ln->intensity == ln->intensity);
  CHK(ln->gamma_air == ln->gamma_air);
  CHK(ln->gamma_self == ln->gamma_self);
  CHK(ln->lower_state_energy == ln->lower_state_energy);
  CHK(ln->n_air == ln->n_air);
  CHK(ln->delta_air == ln->delta_air);

  CHK(ln->wavenumber > 0);
  CHK(ln->intensity > 0);
  CHK(ln->gamma_air >= 0);
  CHK(ln->gamma_self >= 0);
  CHK(ln->lower_state_energy >= 0);
  CHK(ln->molecule_id >= 0 && ln->molecule_id < 100);
  CHK(ln->isotope_id_local >= 0 && ln->isotope_id_local <= 9);
}

static void
test_load_file(struct shtr* shtr, const char* path)
{
  struct shtr_line_list* list = NULL;
  const struct shtr_line* lines = NULL;
  size_t i, n;
  CHK(path);
  printf("Loading `%s'.\n", path);
  CHK(shtr_line_list_load(shtr, path, &list) == RES_OK);
  CHK(shtr_line_list_get_size(list, &n) == RES_OK);
  printf("  #lines: %lu\n", n);

  CHK(shtr_line_list_get(list, &lines) == RES_OK);
  FOR_EACH(i, 0, n) check_line(lines+i);
  CHK(shtr_line_list_ref_put(list) == RES_OK);
}

int
main(int argc, char** argv)
{
  struct shtr_create_args args = SHTR_CREATE_ARGS_DEFAULT;
  struct shtr* shtr = NULL;
  int i;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(shtr_create(&args, &shtr) == RES_OK);

  test_line_eq();
  test_load(shtr);
  test_load_failures(shtr);
  test_serialization(shtr);
  test_view(shtr);
  test_view_serialization(shtr);

  FOR_EACH(i, 1, argc) {
    char buf[64];
    struct time t0, t1;

    time_current(&t0);
    test_load_file(shtr, argv[i]);
    time_sub(&t0, time_current(&t1), &t0);
    time_dump(&t0, TIME_ALL, NULL, buf, sizeof(buf));
    printf("%s loaded in %s\n", argv[i], buf);
  }

  CHK(shtr_ref_put(shtr) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
