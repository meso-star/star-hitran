/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SHTR_LINE_LIST_C_H
#define SHTR_LINE_LIST_C_H

#include <rsys/dynamic_array.h>
#include <rsys/ref_count.h>

/* Generate the dynamic array of lines */
#define DARRAY_NAME line
#define DARRAY_DATA struct shtr_line
#include <rsys/dynamic_array.h>

struct shtr;

/* Version of the line list. One should increment it and perform a version
 * management onto serialized data when the line list structure is updated. */
static const int SHTR_LINE_LIST_VERSION = 0;

struct shtr_line_list {
  /* Lines sorted in ascending order wrt their wavenumber */
  struct darray_line lines;

  struct shtr* shtr;
  ref_T ref;
};

#endif /* SHTR_LINE_LIST_C_H */
