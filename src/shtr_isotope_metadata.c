/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* strtok_r support */

#include "shtr.h"
#include "shtr_c.h"
#include "shtr_log.h"
#include "shtr_param.h"

#include <rsys/cstr.h>
#include <rsys/dynamic_array_char.h>
#include <rsys/ref_count.h>
#include <rsys/str.h>
#include <rsys/text_reader.h>

#include <ctype.h>
#include <string.h>

/* Version of the isotope metadata. One should increment it and perform a
 * version management onto serialized data when the isotope metadata structure
 * is updated. */
static const int SHTR_ISOTOPE_METADATA_VERSION = 0;

/* Generate the dynamic array of isotopes */
#define DARRAY_NAME isotope
#define DARRAY_DATA struct shtr_isotope
#include <rsys/dynamic_array.h>

struct molecule {
  struct str name;
  size_t isotopes_range[2]; /* Range of the [1st and last[ isotopes */
  int id; /* Unique identifier of the molecule */
};
#define MOLECULE_IS_VALID(Molecule) ((Molecule)->id >= 0)

static INLINE void
molecule_clear(struct molecule* molecule)
{
  ASSERT(molecule);
  molecule->isotopes_range[0] = SIZE_MAX;
  molecule->isotopes_range[1] = 0;
  molecule->id = -1;
}

static INLINE void
molecule_init(struct mem_allocator* allocator, struct molecule* molecule)
{
  str_init(allocator, &molecule->name);
  molecule_clear(molecule);
}

static INLINE void
molecule_release(struct molecule* molecule)
{
  str_release(&molecule->name);
}

static INLINE res_T
molecule_copy(struct molecule* dst, const struct molecule* src)
{
  dst->isotopes_range[0] = src->isotopes_range[0];
  dst->isotopes_range[1] = src->isotopes_range[1];
  dst->id = src->id;
  return str_copy(&dst->name, &src->name);
}

static INLINE res_T
molecule_copy_and_release(struct molecule* dst, struct molecule* src)
{
  dst->isotopes_range[0] = src->isotopes_range[0];
  dst->isotopes_range[1] = src->isotopes_range[1];
  dst->id = src->id;
  return str_copy_and_release(&dst->name, &src->name);
}

/* Generate the dynamic array of molecules */
#define DARRAY_NAME molecule
#define DARRAY_DATA struct molecule
#define DARRAY_FUNCTOR_INIT molecule_init
#define DARRAY_FUNCTOR_RELEASE molecule_release
#define DARRAY_FUNCTOR_COPY molecule_copy
#define DARRAY_FUNCTOR_COPY_AND_RELEASE molecule_copy_and_release
#include <rsys/dynamic_array.h>

struct shtr_isotope_metadata {
  /* List of molecules and isotopes */
  struct darray_molecule molecules;
  struct darray_isotope isotopes;

  /* Map the global identifier of a molecule to its correspond local index into
   * the dynamic aray into which it is registered */
  int molid2idx[SHTR_MAX_MOLECULES_COUNT];

  struct shtr* shtr;
  ref_T ref;
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
create_isotope_metadata
  (struct shtr* shtr,
   struct shtr_isotope_metadata** out_isotopes)
{
  struct shtr_isotope_metadata* metadata = NULL;
  res_T res = RES_OK;
  ASSERT(shtr && out_isotopes);

  metadata = MEM_CALLOC(shtr->allocator, 1, sizeof(*metadata));
  if(!metadata) {
    log_err(shtr,
      "Could not allocate the isotope metadata data structure.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&metadata->ref);
  SHTR(ref_get(shtr));
  metadata->shtr = shtr;
  darray_molecule_init(shtr->allocator, &metadata->molecules);
  darray_isotope_init(shtr->allocator, &metadata->isotopes);
  memset(metadata->molid2idx, 0xFF, sizeof(metadata->molid2idx));

exit:
  *out_isotopes = metadata;
  return res;
error:
  if(metadata) {
    SHTR(isotope_metadata_ref_put(metadata));
    metadata = NULL;
  }
  goto exit;
}

static res_T
flush_molecule
  (struct shtr_isotope_metadata* metadata,
   struct molecule* molecule, /* Currently parsed molecule */
   struct txtrdr* txtrdr)
{
  size_t ientry = SIZE_MAX;
  size_t* pimolecule = NULL;
  res_T res = RES_OK;
  ASSERT(metadata && molecule && MOLECULE_IS_VALID(molecule));

  /* Fetch _exclusive_ upper bound */
  molecule->isotopes_range[1] =
    darray_isotope_size_get(&metadata->isotopes);
  if(molecule->isotopes_range[0] >= molecule->isotopes_range[1]) {
    log_warn(metadata->shtr,
      "%s:%lu: the %s molecule does not have any isotope.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      str_cget(&molecule->name));
  }

  /* Fetch the index where the molecule is going to be store */
  ientry = darray_molecule_size_get(&metadata->molecules);
  CHK(ientry < SHTR_MAX_MOLECULES_COUNT);

  /* Store the molecule */
  res = darray_molecule_push_back(&metadata->molecules, molecule);
  if(res != RES_OK) {
    log_err(metadata->shtr,
      "%s: error storing the %s molecule -- %s.\n",
      txtrdr_get_name(txtrdr), str_cget(&molecule->name), res_to_cstr(res));
    goto error;
  }

  /* Register the molecule */
  if(metadata->molid2idx[molecule->id] >= 0) {
    const struct molecule* molecule2 = NULL;
    molecule2 = darray_molecule_cdata_get(&metadata->molecules) + *pimolecule;
    log_err(metadata->shtr,
      "%s: cannot register the %s molecule. "
      "The %s molecule has the same identifier %i.\n",
      txtrdr_get_name(txtrdr),
      str_cget(&molecule->name),
      str_cget(&molecule2->name),
      molecule->id);
    res = RES_OK;
    goto error;
  }
  ASSERT((size_t)((int)ientry) == ientry);
  metadata->molid2idx[molecule->id] = (int)ientry;
  molecule_clear(molecule);

exit:
  return res;
error:
  if(ientry != SIZE_MAX) darray_molecule_resize(&metadata->molecules, ientry);
  metadata->molid2idx[molecule->id] = -1;
  goto exit;
}

static res_T
parse_molecule
  (struct shtr_isotope_metadata* metadata,
   struct molecule* molecule,
   struct txtrdr* txtrdr)
{
  char* name = NULL;
  char* id = NULL;
  char* tk = NULL;
  char* tk_ctx = NULL;
  size_t len;
  res_T res = RES_OK;
  ASSERT(molecule && txtrdr);

  name = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  id = strtok_r(NULL, " \t", &tk_ctx);

  if(!name) {
    log_err(metadata->shtr, "%s:%lu: molecule name is missing.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  res = str_set(&molecule->name, name);
  if(res != RES_OK) {
    log_err(metadata->shtr,
      "%s:%lu: error seting the molecule name `%s' -- %s.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr),
      name, res_to_cstr(res));
    goto error;
  }

  len = strlen(id);
  if(!id || !len || id[0] != '(' || id[len-1] != ')') {
    log_err(metadata->shtr,
      "%s:%lu: invalid definition of the molecule identifier.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
    res = RES_BAD_ARG;
    goto error;
  }

  id[len-1] = '\0'; /* Rm trailing parenthesis */
  res = cstr_to_int(id+1/*Rm leading parenthesis*/, &molecule->id);
  if(res != RES_OK || !MOLECULE_IS_VALID(molecule)) {
    id[len-1] = ')'; /* Re-add the trailing parenthesis */
    log_err(metadata->shtr, "%s:%lu: invalid molecule identifier `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), id);
    res = RES_BAD_ARG;
    goto error;
  }

  tk = strtok_r(NULL, " \t", &tk_ctx);
  if(tk) {
    log_warn(metadata->shtr, "%s:%lu: unexpected text `%s'.\n",
      txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr), tk);
  }

  molecule->isotopes_range[0] = darray_isotope_size_get(&metadata->isotopes);

exit:
  return res;
error:
  goto exit;
}

static res_T
parse_isotope
  (struct shtr_isotope_metadata* metadata,
   struct txtrdr* txtrdr)
{
  struct shtr_isotope isotope = SHTR_ISOTOPE_NULL;
  struct param_desc param_desc = PARAM_DESC_NULL;
  struct shtr* shtr = NULL;
  char* tk = NULL;
  char* tk_ctx = NULL;
  size_t local_id = SIZE_MAX;
  res_T res = RES_OK;
  ASSERT(metadata && txtrdr);

  shtr = metadata->shtr;
  param_desc.path = txtrdr_get_name(txtrdr);
  param_desc.line = txtrdr_get_line_num(txtrdr);

  /* Fetch the index of the molecule to which the isotope belongs */
  isotope.molecule_id_local = darray_molecule_size_get(&metadata->molecules);

  tk = strtok_r(txtrdr_get_line(txtrdr), " \t", &tk_ctx);
  param_desc.name = "isotope id";
  param_desc.low = 0;
  param_desc.upp = INT_MAX;
  param_desc.is_low_incl = 1;
  param_desc.is_upp_incl = 1;
  res = parse_param_int(shtr, tk, &param_desc, &isotope.id);
  if(res != RES_OK) goto error;

  tk = strtok_r(NULL, " \t", &tk_ctx);
  param_desc.name = "isotope abundance";
  param_desc.low = 0;
  param_desc.upp = 1;
  param_desc.is_low_incl = 0;
  param_desc.is_upp_incl = 1;
  res = parse_param_double(shtr, tk, &param_desc, &isotope.abundance);
  if(res != RES_OK) goto error;

  tk = strtok_r(NULL, " \t", &tk_ctx);
  param_desc.name = "isotope Q(296K)";
  param_desc.low = 0;
  param_desc.upp = INF;
  param_desc.is_low_incl = 0;
  param_desc.is_upp_incl = 1;
  res = parse_param_double(shtr, tk, &param_desc, &isotope.Q296K);
  if(res !=  RES_OK) goto error;

  tk = strtok_r(NULL, " \t", &tk_ctx);
  param_desc.name = "isotope state independant degeneracy factor";
  param_desc.low = -INT_MAX;
  param_desc.upp =  INT_MAX;
  param_desc.is_low_incl = 1;
  param_desc.is_upp_incl = 1;
  res = parse_param_int(shtr, tk, &param_desc, &isotope.gj);
  if(res != RES_OK) goto error;

  tk = strtok_r(NULL, " \t", &tk_ctx),
  param_desc.name = "isotope molar mass";
  param_desc.low = 0;
  param_desc.upp = INF;
  param_desc.is_low_incl = 0;
  param_desc.is_upp_incl = 1;
  res = parse_param_double(shtr, tk, &param_desc, &isotope.molar_mass);
  if(res != RES_OK) goto error;

  local_id = darray_isotope_size_get(&metadata->isotopes);

  /* Store the isotope */
  res = darray_isotope_push_back(&metadata->isotopes, &isotope);
  if(res != RES_OK) {
    log_err(shtr, "%s:%lu: error storing the isotope %d -- %s.\n",
      param_desc.path, param_desc.line, isotope.id, res_to_cstr(res));
    res = RES_OK;
    goto error;
  }

exit:
  return res;
error:
  if(local_id != SIZE_MAX) darray_isotope_resize(&metadata->isotopes, local_id);
  goto exit;
}

static res_T
parse_line
  (struct shtr_isotope_metadata* metadata,
   struct molecule* molecule, /* Currently parsed molecule */
   struct txtrdr* txtrdr)
{
  const char* line = NULL;
  size_t i;
  res_T res = RES_OK;
  ASSERT(metadata && molecule && txtrdr);

  line = txtrdr_get_cline(txtrdr);
  ASSERT(line);
  i = strspn(line, " \t");
  ASSERT(i < strlen(line));

  if(isalpha(line[i])) {
    if(MOLECULE_IS_VALID(molecule)) {
      res = flush_molecule(metadata, molecule, txtrdr);
      if(res != RES_OK) goto error;
    }
    res = parse_molecule(metadata, molecule, txtrdr);
    if(res != RES_OK) goto error;
  } else {
    if(!MOLECULE_IS_VALID(molecule)) {
      log_err(metadata->shtr, "%s:%lu: missing a molecule definition.\n",
        txtrdr_get_name(txtrdr), (unsigned long)txtrdr_get_line_num(txtrdr));
      res = RES_BAD_ARG;
      goto error;
    }
    res = parse_isotope(metadata, txtrdr);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
load_stream
  (struct shtr* shtr,
   FILE* stream,
   const char* name,
   struct shtr_isotope_metadata** out_isotopes)
{
  struct molecule molecule; /* Current molecule */
  struct shtr_isotope_metadata* metadata = NULL;
  struct txtrdr* txtrdr = NULL;
  res_T res = RES_OK;
  ASSERT(shtr && stream && name && out_isotopes);

  molecule_init(shtr->allocator, &molecule);

  res = create_isotope_metadata(shtr, &metadata);
  if(res != RES_OK) goto error;

  res = txtrdr_stream(metadata->shtr->allocator, stream, name,
    0/*No comment char*/, &txtrdr);
  if(res != RES_OK) {
    log_err(shtr, "%s: error creating the text reader -- %s.\n",
      name, res_to_cstr(res));
    goto error;
  }

  #define READ_LINE {                                                          \
    res = txtrdr_read_line(txtrdr);                                            \
    if(res != RES_OK) {                                                        \
      log_err(shtr, "%s: error reading the line `%lu' -- %s.\n",               \
        name, (unsigned long)txtrdr_get_line_num(txtrdr), res_to_cstr(res));   \
      goto error;                                                              \
    }                                                                          \
  } (void)0

  /* Skip the 1st line that is a comment line*/
  READ_LINE;
  if(!txtrdr_get_cline(txtrdr)) goto exit;

  for(;;) {
    READ_LINE;

    if(!txtrdr_get_cline(txtrdr)) break; /* No more parsed line */
    res = parse_line(metadata, &molecule, txtrdr);
    if(res != RES_OK) goto error;
  }
  #undef READ_LINE

  if(MOLECULE_IS_VALID(&molecule)) {
    res = flush_molecule(metadata, &molecule, txtrdr);
    if(res != RES_OK) goto error;
  }

exit:
  if(txtrdr) txtrdr_ref_put(txtrdr);
  *out_isotopes = metadata;
  molecule_release(&molecule);
  return res;
error:
  if(metadata) {
    SHTR(isotope_metadata_ref_put(metadata));
    metadata = NULL;
  }
  goto exit;
}

static res_T
write_molecules
  (const struct shtr_isotope_metadata* metadata,
   const char* caller,
   FILE* stream)
{
  const struct molecule* molecules = NULL;
  size_t i, nmolecules;
  res_T res = RES_OK;
  ASSERT(metadata && caller && stream);

  molecules = darray_molecule_cdata_get(&metadata->molecules);
  nmolecules = darray_molecule_size_get(&metadata->molecules);

  #define WRITE(Var, Nb) {                                                     \
    if(fwrite((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                  \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&nmolecules, 1);

  FOR_EACH(i, 0, nmolecules) {
    const struct molecule* molecule = molecules + i;
    const size_t len = str_len(&molecule->name);
    WRITE(&len, 1);
    WRITE(str_cget(&molecule->name), len);
    WRITE(molecule->isotopes_range, 2);
    WRITE(&molecule->id, 1);
  }
  #undef WRITE

exit:
  return res;
error:
  log_err(metadata->shtr, "%s: error writing molecules\n", caller);
  goto exit;
}

static res_T
read_molecules
  (struct shtr_isotope_metadata* metadata,
   const char* caller,
   FILE* stream)
{
  struct darray_char name;
  struct molecule* molecules = NULL;
  size_t i, nmolecules;
  res_T res = RES_OK;
  ASSERT(metadata && caller && stream);

  darray_char_init(metadata->shtr->allocator, &name);

  #define READ(Var, Nb) {                                                      \
    if(fread((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                   \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0

  READ(&nmolecules, 1);

  res = darray_molecule_resize(&metadata->molecules, nmolecules);
  if(res != RES_OK) goto error;

  molecules = darray_molecule_data_get(&metadata->molecules);
  FOR_EACH(i, 0, nmolecules) {
    struct molecule* molecule = molecules + i;
    size_t len = 0;

    READ(&len, 1);
    res = darray_char_resize(&name, len+1);
    if(res != RES_OK) goto error;

    READ(darray_char_data_get(&name), len);
    darray_char_data_get(&name)[len] = '\0';
    res = str_set(&molecule->name, darray_char_data_get(&name));
    if(res != RES_OK) goto error;

    READ(molecule->isotopes_range, 2);
    READ(&molecule->id, 1);
  }
  #undef READ

exit:
  darray_char_release(&name);
  return res;
error:
  log_err(metadata->shtr, "%s: error reading molecules -- %s.\n",
    caller, res_to_cstr(res));
  goto exit;
}

static res_T
write_isotopes
  (const struct shtr_isotope_metadata* metadata,
   const char* caller,
   FILE* stream)
{
  const struct shtr_isotope* isotopes = NULL;
  size_t nisotopes = 0;
  res_T res = RES_OK;
  ASSERT(metadata && caller && stream);

  isotopes = darray_isotope_cdata_get(&metadata->isotopes);
  nisotopes = darray_isotope_size_get(&metadata->isotopes);

  #define WRITE(Var, Nb) {                                                     \
    if(fwrite((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                  \
      log_err(metadata->shtr, "%s: error writing isotopes\n", caller);         \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&nisotopes, 1);
  WRITE(isotopes, nisotopes);
  #undef WRITE

exit:
  return res;
error:
  goto exit;
}

static res_T
read_isotopes
  (struct shtr_isotope_metadata* metadata,
   const char* caller,
   FILE* stream)
{
  size_t nisotopes = 0;
  res_T res = RES_OK;
  ASSERT(metadata && caller && stream);

  #define READ(Var, Nb) {                                                      \
    if(fread((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                   \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&nisotopes, 1);
  res = darray_isotope_resize(&metadata->isotopes, nisotopes);
  if(res != RES_OK) goto error;
  READ(darray_isotope_data_get(&metadata->isotopes), nisotopes);
  #undef READ

exit:
  return res;
error:
  log_err(metadata->shtr, "%s: error reading isotopes -- %s.\n",
    caller, res_to_cstr(res));
  goto exit;
}

static void
release_isotope_metadata(ref_T* ref)
{
  struct shtr* shtr = NULL;
  struct shtr_isotope_metadata* metadata = CONTAINER_OF
    (ref, struct shtr_isotope_metadata, ref);
  ASSERT(ref);
  shtr = metadata->shtr;
  darray_molecule_release(&metadata->molecules);
  darray_isotope_release(&metadata->isotopes);
  MEM_RM(shtr->allocator, metadata);
  SHTR(ref_put(shtr));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
shtr_isotope_metadata_load
  (struct shtr* shtr,
   const char* path,
   struct shtr_isotope_metadata** metadata)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!shtr || !path || !metadata) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(shtr, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(shtr, file, path, metadata);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
shtr_isotope_metadata_load_stream
  (struct shtr* shtr,
   FILE* stream,
   const char* stream_name,
   struct shtr_isotope_metadata** metadata)
{
  if(!shtr || !stream || !metadata) return RES_BAD_ARG;
  return load_stream
    (shtr, stream, stream_name ? stream_name : "<stream>", metadata);
}

res_T
shtr_isotope_metadata_create_from_stream
  (struct shtr* shtr,
   FILE* stream,
   struct shtr_isotope_metadata** out_metadata)
{
  struct shtr_isotope_metadata* metadata = NULL;
  int version = 0;
  res_T res = RES_OK;

  if(!shtr || !out_metadata || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = create_isotope_metadata(shtr, &metadata);
  if(res != RES_OK) goto error;

  #define READ(Var, Nb) {                                                      \
    if(fread((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                   \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      log_err(shtr, "%s: error reading isotope metadata -- %s.\n",             \
        FUNC_NAME, res_to_cstr(res));                                          \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&version, 1);
  if(version != SHTR_ISOTOPE_METADATA_VERSION) {
    log_err(shtr,
      "%s: unexpected isotope metadata version %d. "
      "Expecting a isotope metadata in version %d.\n",
      FUNC_NAME, version, SHTR_ISOTOPE_METADATA_VERSION);
    res = RES_BAD_ARG;
    goto error;
  }

  res = read_molecules(metadata, FUNC_NAME, stream);
  if(res != RES_OK) goto error;
  res = read_isotopes(metadata, FUNC_NAME, stream);
  if(res != RES_OK) goto error;

  READ(metadata->molid2idx, SHTR_MAX_MOLECULES_COUNT);
  #undef READ

exit:
  if(out_metadata) *out_metadata = metadata;
  return res;
error:
  if(metadata) {
    SHTR(isotope_metadata_ref_put(metadata));
    metadata = NULL;
  }
  goto exit;
}

res_T
shtr_isotope_metadata_ref_get
  (struct shtr_isotope_metadata* metadata)
{
  if(!metadata) return RES_BAD_ARG;
  ref_get(&metadata->ref);
  return RES_OK;
}

res_T
shtr_isotope_metadata_ref_put
  (struct shtr_isotope_metadata* metadata)
{
  if(!metadata) return RES_BAD_ARG;
  ref_put(&metadata->ref, release_isotope_metadata);
  return RES_OK;
}

res_T
shtr_isotope_metadata_get_molecules_count
  (const struct shtr_isotope_metadata* metadata,
   size_t* nmolecules)
{
  if(!metadata || !nmolecules) return RES_BAD_ARG;
  *nmolecules = darray_molecule_size_get(&metadata->molecules);
  return RES_OK;
}

res_T
shtr_isotope_metadata_get_isotopes_count
  (const struct shtr_isotope_metadata* metadata,
   size_t* nisotopes)
{
  if(!metadata || !nisotopes) return RES_BAD_ARG;
  *nisotopes = darray_isotope_size_get(&metadata->isotopes);
  return RES_OK;
}

res_T
shtr_isotope_metadata_get_molecule
  (const struct shtr_isotope_metadata* metadata,
   const size_t imolecule,
   struct shtr_molecule* out_molecule)
{
  const struct molecule* molecule = NULL;
  res_T res = RES_OK;

  if(!metadata || !out_molecule) {
    res = RES_BAD_ARG;
    goto error;
  }
  if(imolecule >= darray_molecule_size_get(&metadata->molecules)) {
    log_err(metadata->shtr, "%s: invalid molecule index %lu.\n",
      FUNC_NAME, (unsigned long)imolecule);
    res = RES_BAD_ARG;
    goto error;
  }

  molecule = darray_molecule_cdata_get(&metadata->molecules) + imolecule;
  out_molecule->name = str_cget(&molecule->name);
  out_molecule->id = molecule->id;
  out_molecule->nisotopes =
    molecule->isotopes_range[1] - molecule->isotopes_range[0];
  out_molecule->isotopes =
    darray_isotope_cdata_get(&metadata->isotopes) + molecule->isotopes_range[0];

exit:
  return res;
error:
  goto exit;
}

res_T
shtr_isotope_metadata_find_molecule
  (struct shtr_isotope_metadata* metadata,
   const int molecule_id,
   struct shtr_molecule* out_molecule)
{
  int imolecule = 0;
  res_T res = RES_OK;

  if(!metadata || !out_molecule) {
    res = RES_BAD_ARG;
    goto error;
  }

  imolecule = metadata->molid2idx[molecule_id];
  if(imolecule < 0) {
    *out_molecule = SHTR_MOLECULE_NULL;
  } else {
    res = shtr_isotope_metadata_get_molecule
      (metadata, (size_t)imolecule, out_molecule);
    if(res != RES_OK) goto error;
  }

exit:
  return res;
error:
  goto exit;
}

res_T
shtr_isotope_metadata_write
  (const struct shtr_isotope_metadata* metadata,
   FILE* stream)
{
  res_T res = RES_OK;

  if(!metadata || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  #define WRITE(Var, Nb) {                                                     \
    if(fwrite((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                  \
      log_err(metadata->shtr, "%s: error writing metadata\n", FUNC_NAME);      \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&SHTR_ISOTOPE_METADATA_VERSION, 1);

  res = write_molecules(metadata, FUNC_NAME, stream);
  if(res != RES_OK) goto error;
  res = write_isotopes(metadata, FUNC_NAME, stream);
  if(res != RES_OK) goto error;

  WRITE(metadata->molid2idx, SHTR_MAX_MOLECULES_COUNT);
  #undef WRITE

exit:
  return res;
error:
  goto exit;
}
