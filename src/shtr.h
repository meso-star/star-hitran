/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SHTR_H
#define SHTR_H

#include <rsys/rsys.h>

/* Library symbol management */
#if defined(SHTR_SHARED_BUILD)  /* Build shared library */
  #define SHTR_API extern EXPORT_SYM
#elif defined(SHTR_STATIC)  /* Use/build static library */
  #define SHTR_API extern LOCAL_SYM
#else
  #define SHTR_API extern IMPORT_SYM
#endif

/* Helper macro that asserts if the invocation of the shtr function `Func'
 * returns an error. One should use this macro on shtr function calls for which
 * no explicit error checking is performed */
#ifndef NDEBUG
  #define SHTR(Func) ASSERT(shtr_ ## Func == RES_OK)
#else
  #define SHTR(Func) shtr_ ## Func
#endif

#define SHTR_MAX_MOLECULES_COUNT 100
#define SHTR_MAX_ISOTOPES_COUNT 10

struct shtr_isotope {
  double abundance; /* in ]0, 1] */
  double Q296K; /* Partition function at Tref = 296K */
  double molar_mass; /* In g.mol^-1 */

  /* Local idx of the molecule to which the isotope belongs */
  size_t molecule_id_local;

  int gj; /* State independent degeneracy factor */
  int id; /* Identifier of the isotope <=> Global index */
};
#define SHTR_ISOTOPE_NULL__ {0,0,0,0,0,-1}
static const struct shtr_isotope SHTR_ISOTOPE_NULL =
  SHTR_ISOTOPE_NULL__;

struct shtr_molecule {
  const char* name;
  size_t nisotopes; /* Number of isotopes */
  const struct shtr_isotope* isotopes;
  int id; /* Unique identifier */
};
#define SHTR_MOLECULE_NULL__ {NULL, 0, NULL, -1}
static const struct shtr_molecule SHTR_MOLECULE_NULL =
  SHTR_MOLECULE_NULL__;

#define SHTR_MOLECULE_IS_NULL(Molecule) \
  ((Molecule)->id == SHTR_MOLECULE_NULL.id)

struct shtr_line {
  double wavenumber; /* Central wavenumber in vacuum [cm^-1] */
  double intensity; /* Reference intensity [cm^-1/(molec.cm^2)] */
  double gamma_air; /* Air broadening half-width [cm^-1.atm^-1] */
  double gamma_self; /* Self broadening half-width [cm^-1.atm^-1] */
  double lower_state_energy; /* [cm^-1] */
  double n_air; /* Temperature-dependant exponent */
  double delta_air; /* Air-pressure wavenumber shift [cm^-1.atm^-1] */

  int32_t molecule_id;

  /* The value of the following isotopic index is _not_ the value of the
   * isotopic index read from the HITRAN file. The original value is in [0, 9]
   * with 0 actually meaning 10. Thus, once decoded, the index is located in
   * [1, 10]. The next member variable simply stores this index but decremented
   * by one in order to make it compatible with C indexing. As a result, it
   * can be used directly to index the 'isotopes' array of a 'shtr_molecule'
   * data structure loaded from an isotope metadata file */
  int32_t isotope_id_local;
};
#define SHTR_LINE_NULL__ {0,0,0,0,0,0,0,-1,-1}
static const struct shtr_line SHTR_LINE_NULL = SHTR_LINE_NULL__;

static INLINE int
shtr_line_eq(const struct shtr_line* line0, const struct shtr_line* line1)
{
  ASSERT(line0 && line1);
  return line0->wavenumber == line1->wavenumber
      && line0->intensity == line1->intensity
      && line0->gamma_air == line1->gamma_air
      && line0->gamma_self == line1->gamma_self
      && line0->lower_state_energy == line1->lower_state_energy
      && line0->n_air == line1->n_air
      && line0->delta_air == line1->delta_air
      && line0->molecule_id == line1->molecule_id
      && line0->isotope_id_local == line1->isotope_id_local;
}

/* Forward declarations of opaque data structures */
struct shtr;
struct shtr_isotope_metadata;
struct shtr_line_list;
struct shtr_line_view;

/*******************************************************************************
 * Input arguments for API functions
 ******************************************************************************/
struct shtr_create_args {
  struct logger* logger; /* May be NULL <=> default logger */
  struct mem_allocator* allocator; /* NULL <=> use default allocator */
  int verbose; /* Verbosity level */
};
#define SHTR_CREATE_ARGS_DEFAULT__ {0}
static const struct shtr_create_args SHTR_CREATE_ARGS_DEFAULT =
  SHTR_CREATE_ARGS_DEFAULT__;

struct shtr_isotope_selection {
  /* List of isotopes identifier to consider for this molecule. The listed
   * idenfiers are _local_ to the molecule */
  int32_t isotope_ids_local[SHTR_MAX_ISOTOPES_COUNT];
  size_t nisotopes; /* 0 <=> select all the isotopes */

  int32_t id; /* Molecule to which the isotopes belongs */
  double cutoff; /* In cm^-1 */
};
#define SHTR_ISOTOPE_SELECTION_NULL__ {{0}, 0, 0, 0}
static const struct shtr_isotope_selection
SHTR_ISOTOPE_SELECTION_NULL = SHTR_ISOTOPE_SELECTION_NULL__;

struct shtr_line_view_create_args {
  double wavenumber_range[2]; /* Spectral range */

  /* List of molecule be selected */
  struct shtr_isotope_selection molecules[SHTR_MAX_MOLECULES_COUNT];
  size_t nmolecules;

  double pressure; /* In atm. Used to compute the line center */
};
#define SHTR_LINE_VIEW_CREATE_ARGS_NULL__ \
  {{0,0}, {SHTR_ISOTOPE_SELECTION_NULL__}, 0, 0}
static const struct shtr_line_view_create_args
SHTR_LINE_VIEW_CREATE_ARGS_NULL =
  SHTR_LINE_VIEW_CREATE_ARGS_NULL__;

BEGIN_DECLS

/*******************************************************************************
 * Device API
 ******************************************************************************/
SHTR_API res_T
shtr_create
  (const struct shtr_create_args* args,
   struct shtr** shtr);

SHTR_API res_T
shtr_ref_get
  (struct shtr* shtr);

SHTR_API res_T
shtr_ref_put
  (struct shtr* shtr);

/*******************************************************************************
 * Isotope metadata API
 ******************************************************************************/
SHTR_API res_T
shtr_isotope_metadata_load
  (struct shtr* shtr,
   const char* path,
   struct shtr_isotope_metadata** metadata);

SHTR_API res_T
shtr_isotope_metadata_load_stream
  (struct shtr* shtr,
   FILE* stream,
   const char* stream_name, /* NULL <=> use default stream name */
   struct shtr_isotope_metadata** metadata);

/* Load the isotope metadata serialized with the "shtr_isotope_metadata_write"
 * function */
SHTR_API res_T
shtr_isotope_metadata_create_from_stream
  (struct shtr* shtr,
   FILE* stream,
   struct shtr_isotope_metadata** metadata);

SHTR_API res_T
shtr_isotope_metadata_ref_get
  (struct shtr_isotope_metadata* metadata);

SHTR_API res_T
shtr_isotope_metadata_ref_put
  (struct shtr_isotope_metadata* metadata);

SHTR_API res_T
shtr_isotope_metadata_get_molecules_count
  (const struct shtr_isotope_metadata* metadata,
   size_t* nmolecules);

SHTR_API res_T
shtr_isotope_metadata_get_isotopes_count
  (const struct shtr_isotope_metadata* metadata,
   size_t* nisotopes);

SHTR_API res_T
shtr_isotope_metadata_get_molecule
  (const struct shtr_isotope_metadata* metadata,
   const size_t imolecule, /* Local index of the molecule in [0, molecules_count[ */
   struct shtr_molecule* molecule);

/* `molecule' is set to SHTR_MOLECULE_NULL if `molecule_id' is not found */
SHTR_API res_T
shtr_isotope_metadata_find_molecule
  (struct shtr_isotope_metadata* metadata,
   const int molecule_id, /* Unique identifier of the molecule <=> Global index */
   struct shtr_molecule* molecule);

SHTR_API res_T
shtr_isotope_metadata_write
  (const struct shtr_isotope_metadata* metadata,
   FILE* stream);

/*******************************************************************************
 * Lines API
 ******************************************************************************/
SHTR_API res_T
shtr_line_list_load
  (struct shtr* shtr,
   const char* path,
   struct shtr_line_list** list);

SHTR_API res_T
shtr_line_list_load_stream
  (struct shtr* shtr,
   FILE* stream,
   const char* stream_name, /* NULL <=> use default stream name */
   struct shtr_line_list** list);

/* Load the line list serialized with the "shtr_line_list_write" function */
SHTR_API res_T
shtr_line_list_create_from_stream
  (struct shtr* shtr,
   FILE* stream,
   struct shtr_line_list** list);

SHTR_API res_T
shtr_line_list_ref_get
   (struct shtr_line_list* list);

SHTR_API res_T
shtr_line_list_ref_put
   (struct shtr_line_list* list);

SHTR_API res_T
shtr_line_list_get_size
  (const struct shtr_line_list* list,
   size_t* nlines);

SHTR_API res_T
shtr_line_list_get
  (const struct shtr_line_list* list,
   const struct shtr_line* lines[]);

SHTR_API res_T
shtr_line_list_write
  (const struct shtr_line_list* list,
   FILE* stream);

/*******************************************************************************
 * Lines view API
 ******************************************************************************/
SHTR_API res_T
shtr_line_view_create
  (struct shtr_line_list* list,
   const struct shtr_line_view_create_args* args,
   struct shtr_line_view** view);

/* Load the line list serialized with the "shtr_line_view_write" function */
SHTR_API res_T
shtr_line_view_create_from_stream
  (struct shtr* shtr,
   FILE* stream,
   struct shtr_line_view** view);

SHTR_API res_T
shtr_line_view_ref_get
  (struct shtr_line_view* view);

SHTR_API res_T
shtr_line_view_ref_put
  (struct shtr_line_view* view);

SHTR_API res_T
shtr_line_view_get_size
  (const struct shtr_line_view* view,
   size_t* nlines);

SHTR_API res_T
shtr_line_view_get_line
  (const struct shtr_line_view* view,
   const size_t iline,
   const struct shtr_line** line);

SHTR_API res_T
shtr_line_view_write
  (const struct shtr_line_view* view,
   FILE* stream);

END_DECLS

#endif /* SHTR_H */
