/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "shtr.h"

#include <rsys/mem_allocator.h>
#include <rsys/math.h>

#include <string.h>

static const struct shtr_isotope H2O_isotopes[] = {
  {9.97317E-01, 1.7458E+02, 18.010565, 0, 1, 161},
  {1.99983E-03, 1.7605E+02, 20.014811, 0, 1, 181},
  {3.71884E-04, 1.0521E+03, 19.014780, 0, 6, 171},
  {3.10693E-04, 8.6474E+02, 19.016740, 0, 6, 162},
  {6.23003E-07, 8.7557E+02, 21.020985, 0, 6, 182},
  {1.15853E-07, 5.2268E+03, 20.020956, 0, 36, 172},
  {2.41974E-08, 1.0278E+03, 20.022915, 0, 1, 262}
};


static const struct shtr_molecule H2O = {
  "H2O", sizeof(H2O_isotopes)/sizeof(struct shtr_isotope), H2O_isotopes, 1,
};

static const struct shtr_isotope CO2_isotopes[] = {
  {9.84204E-01, 2.8609E+02, 43.989830, 1, 1, 626},
  {1.10574E-02, 5.7664E+02, 44.993185, 1, 2, 636},
  {3.94707E-03, 6.0781E+02, 45.994076, 1, 1, 628},
  {7.33989E-04, 3.5426E+03, 44.994045, 1, 6, 627},
  {4.43446E-05, 1.2255E+03, 46.997431, 1, 2, 638},
  {8.24623E-06, 7.1413E+03, 45.997400, 1, 12, 637},
  {3.95734E-06, 3.2342E+02, 47.998320, 1, 1, 828},
  {1.47180E-06, 3.7666E+03, 46.998291, 1, 6, 827},
  {1.36847E-07, 1.0972E+04, 45.998262, 1, 1, 727},
  {4.44600E-08, 6.5224E+02, 49.001675, 1, 2, 838},
  {1.65354E-08, 7.5950E+03, 48.001646, 1, 12, 837},
  {1.53745E-09, 2.2120E+04, 47.001618, 1, 2, 737}
};

static const struct shtr_molecule CO2 = {
  "CO2", sizeof(CO2_isotopes)/sizeof(struct shtr_isotope), CO2_isotopes, 2,
};

static void
isotope_print(FILE* fp, const struct shtr_isotope* isotope)
{
  CHK(fp && isotope);
  fprintf(fp, "    %d %.5E %.4E %d %.6f\n",
    isotope->id,
    isotope->abundance,
    isotope->Q296K,
    isotope->gj,
    isotope->molar_mass);
}

static void
molecule_print(FILE* fp, const struct shtr_molecule* molecule)
{
  size_t i;
  CHK(fp && molecule);

  fprintf(fp, "  %s (%d)\n", molecule->name, molecule->id);
  FOR_EACH(i, 0, molecule->nisotopes) {
    isotope_print(fp, molecule->isotopes+i);
  }
}

static int
isotope_eq(const struct shtr_isotope* i0, const struct shtr_isotope* i1)
{
  CHK(i0 && i1);
  return i0->abundance == i1->abundance
      && i0->Q296K == i1->Q296K
      && i0->molar_mass == i1->molar_mass
      && i0->molecule_id_local == i1->molecule_id_local
      && i0->gj == i1->gj
      && i0->id == i1->id;
}

static int
molecule_eq(const struct shtr_molecule* m0, const struct shtr_molecule* m1)
{
  size_t i;

  CHK(m0 && m1);
  if(strcmp(m0->name, m1->name)
  || m0->id != m1->id
  || m0->nisotopes != m1->nisotopes)
    return 0;

  FOR_EACH(i, 0, m0->nisotopes) {
    if(!isotope_eq(m0->isotopes+i, m1->isotopes+i))
      return 0;
  }
  return 1;
}

static void
check_isotope
  (struct shtr_isotope_metadata* mdata,
   const struct shtr_molecule* molecule,
   const struct shtr_isotope* isotope)
{
  struct shtr_molecule molecule2 = SHTR_MOLECULE_NULL;

  /* Check NaN */
  CHK(isotope->abundance == isotope->abundance);
  CHK(isotope->Q296K == isotope->Q296K);
  CHK(isotope->molar_mass == isotope->molar_mass);

  CHK(isotope->abundance > 0 && isotope->abundance <= 1);
  CHK(isotope->Q296K > 0);
  CHK(isotope->molar_mass > 0);
  CHK(isotope->id >= 0);

  CHK(shtr_isotope_metadata_get_molecule
    (mdata, isotope->molecule_id_local, &molecule2) == RES_OK);
  CHK(molecule_eq(molecule, &molecule2));
}

static void
check_molecule
  (struct shtr_isotope_metadata* mdata,
   struct shtr_molecule* molecule)
{
  size_t i = 0;

  CHK(mdata && molecule);
  CHK(molecule->name);
  CHK(molecule->id >= 0);

  FOR_EACH(i, 0, molecule->nisotopes) {
    check_isotope(mdata, molecule, molecule->isotopes+i);
  }
}

static void
test_load(struct shtr* shtr)
{
  struct shtr_molecule molecule = SHTR_MOLECULE_NULL;
  const char* filename = "test_isotope_metadata.txt";
  struct shtr_isotope_metadata* mdata = NULL;
  FILE* fp = NULL;
  size_t nmolecules = 0;
  size_t nisotopes = 0;

  CHK(fp = fopen(filename, "w+"));

  fprintf(fp, "Molecule # Iso Abundance Q(296K) gj Molar Mass(g)\n");
  molecule_print(fp, &H2O);
  molecule_print(fp, &CO2);
  rewind(fp);

  CHK(shtr_isotope_metadata_load_stream(NULL, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_load_stream(shtr, NULL, NULL, &mdata) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_OK);

  CHK(shtr_isotope_metadata_get_molecules_count(NULL, &nmolecules) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata, NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata, &nmolecules) == RES_OK);
  CHK(nmolecules == 2);

  CHK(shtr_isotope_metadata_get_isotopes_count(NULL, &nisotopes) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_get_isotopes_count(mdata, NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_get_isotopes_count(mdata, &nisotopes) == RES_OK);
  CHK(nisotopes == H2O.nisotopes + CO2.nisotopes);

  CHK(shtr_isotope_metadata_get_molecule(NULL, 0, &molecule) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_get_molecule(mdata, 2, &molecule) == RES_BAD_ARG);

  CHK(shtr_isotope_metadata_get_molecule(mdata, 0, &molecule) == RES_OK);
  CHK(molecule_eq(&molecule, &H2O));
  check_molecule(mdata, &molecule);

  CHK(shtr_isotope_metadata_get_molecule(mdata, 1, &molecule) == RES_OK);
  CHK(molecule_eq(&molecule, &CO2));
  check_molecule(mdata, &molecule);

  CHK(shtr_isotope_metadata_find_molecule(NULL, 1, &molecule) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_find_molecule(mdata, 1, NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_find_molecule(mdata, 1, &molecule) == RES_OK);
  CHK(!SHTR_MOLECULE_IS_NULL(&molecule));
  CHK(molecule_eq(&molecule, &H2O));

  CHK(shtr_isotope_metadata_find_molecule(mdata, 2, &molecule) == RES_OK);
  CHK(!SHTR_MOLECULE_IS_NULL(&molecule));
  CHK(molecule_eq(&molecule, &CO2));

  CHK(shtr_isotope_metadata_find_molecule(mdata, 0, &molecule) == RES_OK);
  CHK(SHTR_MOLECULE_IS_NULL(&molecule));

  CHK(shtr_isotope_metadata_ref_get(NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_ref_get(mdata) == RES_OK);
  CHK(shtr_isotope_metadata_ref_put(NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_ref_put(mdata) == RES_OK);
  CHK(shtr_isotope_metadata_ref_put(mdata) == RES_OK);

  CHK(fclose(fp) == 0);

  CHK(shtr_isotope_metadata_load(NULL, filename, &mdata) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_load(shtr, NULL, &mdata) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_load(shtr, filename, NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_load(shtr, "no_file", &mdata) == RES_IO_ERR);

  CHK(shtr_isotope_metadata_load(shtr, filename, &mdata) == RES_OK);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata, &nmolecules) == RES_OK);
  CHK(nmolecules == 2);
  CHK(shtr_isotope_metadata_get_molecule(mdata, 0, &molecule) == RES_OK);
  CHK(molecule_eq(&molecule, &H2O));
  CHK(shtr_isotope_metadata_get_molecule(mdata, 1, &molecule) == RES_OK);
  CHK(molecule_eq(&molecule, &CO2));
  CHK(shtr_isotope_metadata_ref_put(mdata) == RES_OK);

  /* Empty file */
  CHK(fp = tmpfile());
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_OK);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata, &nmolecules) == RES_OK);
  CHK(nmolecules == 0);
  CHK(shtr_isotope_metadata_ref_put(mdata) == RES_OK);
  fprintf(fp, "Molecule # Iso Abundance Q(296K) gj Molar Mass(g)\n");
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_OK);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata, &nmolecules) == RES_OK);
  CHK(nmolecules == 0);
  CHK(shtr_isotope_metadata_ref_put(mdata) == RES_OK);
  CHK(fclose(fp) == 0);

  /* Molecule without isotope */
  CHK(fp = tmpfile());
  fprintf(fp, "Molecule # Iso Abundance Q(296K) gj Molar Mass(g)\n");
  fprintf(fp, "%s (%d)\n", H2O.name, H2O.id);
  fprintf(fp, "%s (%d)\n", CO2.name, CO2.id);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_OK);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata, &nmolecules) == RES_OK);
  CHK(nmolecules == 2);

  CHK(shtr_isotope_metadata_get_molecule(mdata, 0, &molecule) == RES_OK);
  CHK(!strcmp(molecule.name, H2O.name));
  CHK(molecule.id == H2O.id);
  CHK(molecule.nisotopes == 0);
  CHK(molecule.isotopes == NULL);

  CHK(shtr_isotope_metadata_get_molecule(mdata, 1, &molecule) == RES_OK);
  CHK(!strcmp(molecule.name, CO2.name));
  CHK(molecule.id == CO2.id);
  CHK(molecule.nisotopes == 0);
  CHK(molecule.isotopes == NULL);

  CHK(shtr_isotope_metadata_ref_put(mdata) == RES_OK);
  CHK(fclose(fp) == 0);
}

static void
check_equality
  (const struct shtr_isotope_metadata* mdata1,
   const struct shtr_isotope_metadata* mdata2)
{
  size_t n1, n2;
  size_t imol, nmol;
  CHK(mdata1 && mdata2);

  CHK(shtr_isotope_metadata_get_molecules_count(mdata1, &n1) == RES_OK);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata2, &n2) == RES_OK);
  CHK(n1 == n2);
  nmol = n1;

  CHK(shtr_isotope_metadata_get_isotopes_count(mdata1, &n1) == RES_OK);
  CHK(shtr_isotope_metadata_get_isotopes_count(mdata2, &n2) == RES_OK);
  CHK(n1 == n2);

  FOR_EACH(imol, 0, nmol) {
    struct shtr_molecule mol1 = SHTR_MOLECULE_NULL;
    struct shtr_molecule mol2 = SHTR_MOLECULE_NULL;
    size_t iiso, niso;

    CHK(shtr_isotope_metadata_get_molecule(mdata1, imol, &mol1) == RES_OK);
    CHK(shtr_isotope_metadata_get_molecule(mdata2, imol, &mol2) == RES_OK);
    CHK(!strcmp(mol1.name, mol2.name));
    CHK(mol1.id == mol2.id);
    CHK(mol1.nisotopes == mol2.nisotopes);
    niso = mol1.nisotopes;

    FOR_EACH(iiso, 0, niso) {
      CHK(mol1.isotopes[iiso].abundance == mol2.isotopes[iiso].abundance);
      CHK(mol1.isotopes[iiso].Q296K == mol2.isotopes[iiso].Q296K);
      CHK(mol1.isotopes[iiso].molar_mass == mol2.isotopes[iiso].molar_mass);
      CHK(mol1.isotopes[iiso].molecule_id_local == mol2.isotopes[iiso].molecule_id_local);
      CHK(mol1.isotopes[iiso].gj == mol2.isotopes[iiso].gj);
      CHK(mol1.isotopes[iiso].id == mol2.isotopes[iiso].id);
    }
  }
}

static void
test_serialization(struct shtr* shtr)
{
  struct shtr_isotope_metadata* mdata1 = NULL;
  struct shtr_isotope_metadata* mdata2 = NULL;
  FILE* fp = NULL;

  CHK(fp = tmpfile());

  fprintf(fp, "Molecule # Iso Abundance Q(296K) gj Molar Mass(g)\n");
  molecule_print(fp, &H2O);
  molecule_print(fp, &CO2);
  rewind(fp);

  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata1) == RES_OK);
  fclose(fp);

  CHK(fp = tmpfile());
  CHK(shtr_isotope_metadata_write(NULL, fp) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_write(mdata1, NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_write(mdata1, fp) == RES_OK);
  rewind(fp);

  CHK(shtr_isotope_metadata_create_from_stream(NULL, fp, &mdata2) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_create_from_stream(shtr, NULL, &mdata2) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_create_from_stream(shtr, fp, NULL) == RES_BAD_ARG);
  CHK(shtr_isotope_metadata_create_from_stream(shtr, fp, &mdata2) == RES_OK);
  fclose(fp);

  check_equality(mdata1, mdata2);

  CHK(shtr_isotope_metadata_ref_put(mdata1) == RES_OK);
  CHK(shtr_isotope_metadata_ref_put(mdata2) == RES_OK);
}

static void
test_load_failures(struct shtr* shtr)
{
  struct shtr_isotope isotope = SHTR_ISOTOPE_NULL;
  struct shtr_isotope_metadata* mdata = NULL;
  FILE* fp = NULL;

  CHK(shtr);

  /* 1st line is missing */
  CHK(fp = tmpfile());
  molecule_print(fp, &H2O);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid molecule id */
  CHK(fp = tmpfile());
  fprintf(fp, "Comment line\n");
  fprintf(fp, "H2O 1\n");
  isotope_print(fp, &H2O_isotopes[0]);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  rewind(fp);
  fprintf(fp, "Comment line\n");
  fprintf(fp, "H2O (-1)\n");
  isotope_print(fp, &H2O_isotopes[0]);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid isotope id */
  CHK(fp = tmpfile());
  fprintf(fp, "Comment line\n");
  fprintf(fp, "H2O (1)\n");
  isotope = H2O_isotopes[0];
  isotope.id = -1;
  isotope_print(fp, &isotope);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid abundance */
  CHK(fp = tmpfile());
  fprintf(fp, "Comment line\n");
  fprintf(fp, "H2O (1)\n");
  isotope = H2O_isotopes[0];
  isotope.abundance = 0;
  isotope_print(fp, &isotope);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);
  CHK(fp = tmpfile());
  fprintf(fp, "Comment line\n");
  fprintf(fp, "H2O (1)\n");
  isotope.abundance = 1.00001;
  isotope_print(fp, &isotope);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid Q(296K) */
  CHK(fp = tmpfile());
  fprintf(fp, "Comment line\n");
  fprintf(fp, "H2O (1)\n");
  isotope = H2O_isotopes[0];
  isotope.Q296K = 0;
  isotope_print(fp, &isotope);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);

  /* Invalid molar mass */
  CHK(fp = tmpfile());
  fprintf(fp, "Comment line\n");
  fprintf(fp, "H2O (1)\n");
  isotope = H2O_isotopes[0];
  isotope.molar_mass = 0;
  isotope_print(fp, &isotope);
  rewind(fp);
  CHK(shtr_isotope_metadata_load_stream(shtr, fp, NULL, &mdata) == RES_BAD_ARG);
  CHK(fclose(fp) == 0);
}

static void
test_load_file(struct shtr* shtr, const char* path)
{
  struct shtr_isotope_metadata* mdata = NULL;
  size_t i, n;
  CHK(path);
  printf("Loading `%s'.\n", path);
  CHK(shtr_isotope_metadata_load(shtr, path, &mdata) == RES_OK);
  CHK(shtr_isotope_metadata_get_molecules_count(mdata, &n) == RES_OK);
  printf("  #molecules: %lu\n", n);
  FOR_EACH(i, 0, n) {
    struct shtr_molecule molecule = SHTR_MOLECULE_NULL;
    CHK(shtr_isotope_metadata_get_molecule(mdata, i, &molecule) == RES_OK);
    printf("  Checking %s\n", molecule.name);
    check_molecule(mdata, &molecule);
  }
  CHK(shtr_isotope_metadata_ref_put(mdata) == RES_OK);
}

int
main(int argc, char** argv)
{
  struct shtr_create_args args = SHTR_CREATE_ARGS_DEFAULT;
  struct shtr* shtr = NULL;
  int i;
  (void)argc, (void)argv;

  args.verbose = 1;
  CHK(shtr_create(&args, &shtr) == RES_OK);

  test_load(shtr);
  test_load_failures(shtr);
  test_serialization(shtr);
  FOR_EACH(i, 1, argc) {
    test_load_file(shtr, argv[i]);
  }

  CHK(shtr_ref_put(shtr) == RES_OK);
  CHK(mem_allocated_size() == 0);
  return 0;
}
