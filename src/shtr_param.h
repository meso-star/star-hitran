/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#ifndef SHTR_PARAM_H
#define SHTR_PARAM_H

#include <rsys/rsys.h>

struct param_desc {
  const char* path; /* Path where the param lies */
  const char* name; /* Name of the param */
  size_t line; /* Line number of the param */

  /* Domain of the param */
  double low, upp;
  int is_low_incl; /* Define if the lower bound is inclusive */
  int is_upp_incl; /* Define if the upper bound is inclusive */
};
#define PARAM_DESC_NULL__ {NULL, NULL, 0, 0, 0, 0, 0}
static const struct param_desc PARAM_DESC_NULL = PARAM_DESC_NULL__;

/* Forware declaration */
struct shtr;

extern LOCAL_SYM res_T
parse_param_int
  (struct shtr* shtr,
   const char* str,
   const struct param_desc* desc,
   int* param);

extern LOCAL_SYM res_T
parse_param_double
  (struct shtr* shtr,
   const char* str,
   const struct param_desc* desc,
   double* param);

#endif /* SHTR_PARAM_H */
