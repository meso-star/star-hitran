/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "shtr_c.h"
#include "shtr_log.h"
#include "shtr_param.h"

#include <rsys/cstr.h>

#define C_FORMAT_double "%g"
#define C_FORMAT_float "%g"
#define C_FORMAT_int "%d"

/*******************************************************************************
 * Local functions
 ******************************************************************************/
#define DEFINE_PARSE_PARAM_FUNCTION(Type)                                      \
  res_T                                                                        \
  parse_param_##Type                                                           \
    (struct shtr* shtr,                                                        \
     const char* str,                                                          \
     const struct param_desc* desc,                                            \
     Type* out_param)                                                          \
  {                                                                            \
    Type param = 0;                                                            \
    res_T res = RES_OK;                                                        \
    ASSERT(shtr && desc && out_param);                                         \
    ASSERT(desc->low < desc->upp                                               \
        || (desc->low == desc->upp && desc->is_low_incl && desc->is_upp_incl));\
                                                                               \
    if(!str) {                                                                 \
      log_err(shtr, "%s:%lu: %s is missing.\n",                                \
        desc->path, (unsigned long)desc->line, desc->name);                    \
      res = RES_BAD_ARG;                                                       \
      goto error;                                                              \
    }                                                                          \
                                                                               \
    res = cstr_to_##Type(str, &param);                                         \
    if(res != RES_OK) {                                                        \
      log_err(shtr, "%s:%lu: invalid %s `%s'.\n",                              \
        desc->path, (unsigned long)desc->line, desc->name, str);               \
      res = RES_BAD_ARG;                                                       \
      goto error;                                                              \
    }                                                                          \
                                                                               \
    if(param < desc->low || (param == desc->low && !desc->is_low_incl)         \
    || param > desc->upp || (param == desc->upp && !desc->is_upp_incl)) {      \
      log_err(shtr,                                                            \
        "%s:%lu: invalid %s `%s'. It must be in "                              \
        "%c"CONCAT(C_FORMAT_, Type)", "CONCAT(C_FORMAT_, Type)"%c.\n",         \
        desc->path, (unsigned long)desc->line, desc->name, str,                \
        desc->is_low_incl ? '[' : ']', (Type)desc->low,                        \
        (Type)desc->upp, desc->is_upp_incl ? ']' : '[');                       \
      res = RES_BAD_ARG;                                                       \
      goto error;                                                              \
    }                                                                          \
                                                                               \
  exit:                                                                        \
    *out_param = param;                                                        \
    return res;                                                                \
  error:                                                                       \
    goto exit;                                                                 \
  }
DEFINE_PARSE_PARAM_FUNCTION(int)
DEFINE_PARSE_PARAM_FUNCTION(double)
#undef DEFINE_PARSE_PARAM_FUNCTION
