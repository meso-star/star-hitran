/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "shtr.h"
#include "shtr_c.h"
#include "shtr_line_list_c.h"
#include "shtr_log.h"
#include "shtr_param.h"

#include <rsys/cstr.h>
#include <rsys/text_reader.h>

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
create_line_list(struct shtr* shtr, struct shtr_line_list** out_list)
{
  struct shtr_line_list* list = NULL;
  res_T res = RES_OK;
  ASSERT(shtr && out_list);

  list = MEM_CALLOC(shtr->allocator, 1, sizeof(*list));
  if(!list) {
    log_err(shtr, "Could not allocate the list of lines.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&list->ref);
  SHTR(ref_get(shtr));
  list->shtr = shtr;
  darray_line_init(shtr->allocator, &list->lines);

exit:
  *out_list = list;
  return res;
error:
  if(list) {
    SHTR(line_list_ref_put(list));
    list = NULL;
  }
  goto exit;
}

static res_T
parse_line(struct shtr_line_list* list, struct txtrdr* txtrdr)
{
  struct shtr_line ln = SHTR_LINE_NULL;
  struct param_desc param = PARAM_DESC_NULL;
  struct shtr* shtr = NULL;
  char* line = NULL;
  char* str = NULL;
  char* end = NULL;
  char backup;
  int molecule_id;
  int isotope_id_local;
  res_T res = RES_OK;

  ASSERT(list && txtrdr);

  line = txtrdr_get_line(txtrdr);
  ASSERT(line);

  shtr = list->shtr;
  param.path = txtrdr_get_name(txtrdr);
  param.line = txtrdr_get_line_num(txtrdr);

  str = end = line;
  backup = str[0];
  #define NEXT(Size) {                                                         \
    *end = backup;                                                             \
    str = end;                                                                 \
    end = str+(Size);                                                          \
    backup = *end;                                                             \
    *end = '\0';                                                               \
  } (void)0
  #define PARSE(Var, Size, Type, Name, Low, Upp, LowIncl, UppIncl) {           \
    NEXT(Size);                                                                \
    param.name = (Name);                                                       \
    param.low = (Low);                                                         \
    param.upp = (Upp);                                                         \
    param.is_low_incl = (LowIncl);                                             \
    param.is_upp_incl = (UppIncl);                                             \
    res = parse_param_##Type(shtr, str, &param, Var);                          \
    if(res != RES_OK) goto error;                                              \
  } (void)0

  PARSE(&molecule_id, 2, int, "molecule identifier", 0,99,1,1);
  ln.molecule_id = (int32_t)molecule_id;

  PARSE(&isotope_id_local, 1, int, "isotope local identifier", 0,9,1,1);
  ln.isotope_id_local = (int32_t)
    (isotope_id_local == 0 ? 9 : (isotope_id_local - 1));

  PARSE(&ln.wavenumber, 12, double, "central wavenumber", 0,INF,0,1);
  PARSE(&ln.intensity, 10, double, "reference intensity", 0,INF,0,1);

  NEXT(10); /* Skip the Enstein coef */

  PARSE(&ln.gamma_air, 5, double, "air broadening half-width", 0,INF,1,1);
  PARSE(&ln.gamma_self, 5, double, "self broadening half-width", 0,INF,1,1);

  /* Handle unavailable lower state energy */
  PARSE(&ln.lower_state_energy, 10, double, "lower state energy",-INF,INF,1,1);
  if(ln.lower_state_energy == -1) {
    log_warn(shtr,
      "%s:%lu: the lower state energy is unavailable for this line, so it is "
      "ignored.\n", txtrdr_get_name(txtrdr), txtrdr_get_line_num(txtrdr));
    goto exit; /* Skip the line */
  }
  /* Check the domain validity */
  if(ln.lower_state_energy < 0) {
    log_err(shtr,
      "%s:%lu: invalid lower state energy %g. It must be in [0, INF].\n",
      txtrdr_get_name(txtrdr), txtrdr_get_line_num(txtrdr),
      ln.lower_state_energy);
    res = RES_BAD_ARG;
    goto error;
  }

  PARSE(&ln.n_air, 4, double, "temperature-dependent exponent",-INF,INF,1,1);
  PARSE(&ln.delta_air, 8, double, "air-pressure wavenumber shift", -INF,INF,1,1);

  /* Skip the remaining values */

  #undef NEXT
  #undef PARSE

  /* Check the size of the remaining data to ensure that there is at least the
   * expected number of bytes wrt the HITRAN fileformat */
  *end = backup;
  str = end;
  if(strlen(str) != 93) {
    log_err(list->shtr, "%s:%lu: missing data after delta air.\n",
      param.path, (unsigned long)param.line);
    res = RES_BAD_ARG;
    goto error;
  }

  if(darray_line_size_get(&list->lines)) {
    const struct shtr_line* last_ln = darray_line_cdata_get(&list->lines)
      + darray_line_size_get(&list->lines) - 1;
    if(last_ln->wavenumber > ln.wavenumber) {
      log_err(list->shtr,
        "%s:%lu: lines are not sorted in ascending order wrt their wavenumber.\n",
        txtrdr_get_name(txtrdr), txtrdr_get_line_num(txtrdr));
      res = RES_BAD_ARG;
      goto error;
    }
  }

  res = darray_line_push_back(&list->lines, &ln);
  if(res != RES_OK) {
    log_err(list->shtr,
      "%s:%lu: error storing the line -- %s.\n",
      param.path, (unsigned long)param.line, res_to_cstr(res));
    goto error;
  }

exit:
  return res;
error:
  goto exit;
}

static res_T
load_stream
  (struct shtr* shtr,
   FILE* stream,
   const char* name,
   struct shtr_line_list** out_lines)
{
  struct shtr_line_list* list = NULL;
  struct txtrdr* txtrdr = NULL;
  res_T res = RES_OK;
  ASSERT(shtr && stream && name && out_lines);

  res = create_line_list(shtr, &list);
  if(res != RES_OK) goto error;

  res = txtrdr_stream(list->shtr->allocator, stream, name,
    0/*No comment char*/, &txtrdr);
  if(res != RES_OK) {
    log_err(shtr, "%s: error creating the text reader -- %s.\n",
      name, res_to_cstr(res));
    goto error;
  }

  for(;;) {
    res = txtrdr_read_line(txtrdr);
    if(res != RES_OK) {
      log_err(shtr, "%s: error reading the line `%lu' -- %s.\n",
        name, (unsigned long)txtrdr_get_line_num(txtrdr), res_to_cstr(res));
      goto error;
    }

    if(!txtrdr_get_cline(txtrdr)) break; /* No more parsed line */
    res = parse_line(list, txtrdr);
    if(res != RES_OK) goto error;
  }

exit:
  if(txtrdr) txtrdr_ref_put(txtrdr);
  *out_lines = list;
  return res;
error:
  if(list) {
    SHTR(line_list_ref_put(list));
    list = NULL;
  }
  goto exit;
}

static void
release_lines(ref_T * ref)
{
  struct shtr* shtr = NULL;
  struct shtr_line_list* list = CONTAINER_OF
    (ref, struct shtr_line_list, ref);
  ASSERT(ref);
  shtr = list->shtr;
  darray_line_release(&list->lines);
  MEM_RM(shtr->allocator, list);
  SHTR(ref_put(shtr));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
shtr_line_list_load
  (struct shtr* shtr,
   const char* path,
   struct shtr_line_list** list)
{
  FILE* file = NULL;
  res_T res = RES_OK;

  if(!shtr || !path || !list) {
    res = RES_BAD_ARG;
    goto error;
  }

  file = fopen(path, "r");
  if(!file) {
    log_err(shtr, "%s: error opening file `%s'.\n", FUNC_NAME, path);
    res = RES_IO_ERR;
    goto error;
  }

  res = load_stream(shtr, file, path, list);
  if(res != RES_OK) goto error;

exit:
  if(file) fclose(file);
  return res;
error:
  goto exit;
}

res_T
shtr_line_list_load_stream
  (struct shtr* shtr,
   FILE* stream,
   const char* stream_name,
   struct shtr_line_list** list)
{
  if(!shtr || !stream || !list) return RES_BAD_ARG;
  return load_stream
    (shtr, stream, stream_name ? stream_name : "<stream>", list);
}

res_T
shtr_line_list_create_from_stream
  (struct shtr* shtr,
   FILE* stream,
   struct shtr_line_list** out_list)
{
  struct shtr_line_list* list = NULL;
  size_t nlines;
  int version = 0;
  res_T res = RES_OK;

  if(!shtr || !out_list || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  res = create_line_list(shtr, &list);
  if(res != RES_OK) goto error;

  #define READ(Var, Nb) {                                                      \
    if(fread((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                   \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      log_err(shtr, "%s: error reading isotope metadata -- %s.\n",             \
        FUNC_NAME, res_to_cstr(res));                                          \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&version, 1);
  if(version != SHTR_LINE_LIST_VERSION) {
    log_err(shtr,
      "%s: unexpected line list version %d. "
      "Expecting a line list in version %d.\n",
      FUNC_NAME, version, SHTR_LINE_LIST_VERSION);
    res = RES_BAD_ARG;
    goto error;
  }

  READ(&nlines, 1);
  res = darray_line_resize(&list->lines, nlines);
  if(res != RES_OK) {
    log_err(shtr, "%s: error allocating the line list -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }

  READ(darray_line_data_get(&list->lines), nlines);
  #undef READ

exit:
  if(out_list) *out_list = list;
  return res;
error:
  if(list) {
    SHTR(line_list_ref_put(list));
    list = NULL;
  }
  goto exit;
}

res_T
shtr_line_list_ref_get(struct shtr_line_list* list)
{
  if(!list) return RES_BAD_ARG;
  ref_get(&list->ref);
  return RES_OK;
}

res_T
shtr_line_list_ref_put(struct shtr_line_list* list)
{
  if(!list) return RES_BAD_ARG;
  ref_put(&list->ref, release_lines);
  return RES_OK;
}

res_T
shtr_line_list_get_size
  (const struct shtr_line_list* list,
   size_t* nlines)
{
  if(!list || !nlines) return RES_BAD_ARG;
  *nlines = darray_line_size_get(&list->lines);
  return RES_OK;
}

res_T
shtr_line_list_get
  (const struct shtr_line_list* list,
   const struct shtr_line* line_list[])
{
  if(!list || !line_list) return RES_BAD_ARG;
  *line_list = darray_line_cdata_get(&list->lines);
  return RES_OK;
}

res_T
shtr_line_list_write
  (const struct shtr_line_list* list,
   FILE* stream)
{
  size_t nlines = 0;
  res_T res = RES_OK;

  if(!list || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  nlines = darray_line_size_get(&list->lines);

  #define WRITE(Var, Nb) {                                                     \
    if(fwrite((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                  \
      log_err(list->shtr, "%s: error writing line list.\n", FUNC_NAME);       \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&SHTR_LINE_LIST_VERSION, 1);
  WRITE(&nlines, 1);
  WRITE(darray_line_cdata_get(&list->lines), nlines);
  #undef WRITE

exit:
  return res;
error:
  goto exit;
}
