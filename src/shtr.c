/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)  
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "shtr.h"
#include "shtr_c.h"
#include "shtr_log.h"

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static INLINE res_T
check_shtr_create_args(const struct shtr_create_args* args)
{
  return args ? RES_OK : RES_BAD_ARG;
}

static void
release_shtr(ref_T* ref)
{
  struct shtr* shtr = CONTAINER_OF(ref, struct shtr, ref);
  ASSERT(ref);
  if(shtr->logger == &shtr->logger__) logger_release(&shtr->logger__);
  MEM_RM(shtr->allocator, shtr);
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
shtr_create
  (const struct shtr_create_args* args,
   struct shtr** out_shtr)
{
  struct mem_allocator* allocator = NULL;
  struct shtr* shtr = NULL;
  res_T res = RES_OK;

  if(!out_shtr) { res = RES_BAD_ARG; goto error; }
  res = check_shtr_create_args(args);
  if(res != RES_OK) goto error;

  allocator = args->allocator ? args->allocator : &mem_default_allocator;
  shtr = MEM_CALLOC(allocator, 1, sizeof(*shtr));
  if(!shtr) {
    #define ERR_STR "Could not allocate the Star-HITRAN data structure.\n"
    if(args->logger) {
      logger_print(args->logger, LOG_ERROR, ERR_STR);
    } else {
      fprintf(stderr, MSG_ERROR_PREFIX ERR_STR);
    }
    #undef ERR_STR
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&shtr->ref);
  shtr->allocator = allocator;
  shtr->verbose = args->verbose;
  if(args->logger) {
    shtr->logger = args->logger;
  } else {
    setup_log_default(shtr);
  }

exit:
  if(out_shtr) *out_shtr = shtr;
  return res;
error:
  if(shtr) { SHTR(ref_put(shtr)); shtr = NULL; }
  goto exit;
}

res_T
shtr_ref_get(struct shtr* shtr)
{
  if(!shtr) return RES_BAD_ARG;
  ref_get(&shtr->ref);
  return RES_OK;
}

res_T
shtr_ref_put(struct shtr* shtr)
{
  if(!shtr) return RES_BAD_ARG;
  ref_put(&shtr->ref, release_shtr);
  return RES_OK;
}
