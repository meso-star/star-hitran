/* Copyright (C) 2022 CNRS - LMD
 * Copyright (C) 2022 |Meso|Star> (contact@meso-star.com)
 * Copyright (C) 2022 Université Paul Sabatier - IRIT
 * Copyright (C) 2022 Université Paul Sabatier - Laplace
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>. */

#define _POSIX_C_SOURCE 200112L /* nextafter */

#include "shtr.h"
#include "shtr_c.h"
#include "shtr_line_list_c.h"
#include "shtr_log.h"

#include <rsys/cstr.h>
#include <rsys/dynamic_array_size_t.h>

#include <math.h>

/* Version of the line view. One should increment it and perform a version
 * management onto serialized data when the line view structure is updated. */
static const int SHTR_LINE_VIEW_VERSION = 0;

struct shtr_line_view {
  struct shtr_line_list* list;
  struct darray_size_t line_ids; /* Indices of the selected lines */
  ref_T ref;
};

struct molecule_selection {
  /* Map the isotope local identifier to a boolean defining if the isotope is
   * selected or not */
  char isotopes[SHTR_MAX_ISOTOPES_COUNT];
  double cutoff; /* Molecule cutoff */
};

/*******************************************************************************
 * Helper functions
 ******************************************************************************/
static res_T
check_shtr_isotope_selection
  (struct shtr* shtr,
   const char* caller,
   const struct shtr_isotope_selection* molecule)
{
  size_t i;
  ASSERT(caller && molecule);

  if((size_t)molecule->id >= SHTR_MAX_MOLECULES_COUNT) {
    log_err(shtr,
      "%s: molecule %d: invalid molecule identifier. "
      "It must be less than %d.\n",
      caller, molecule->id, SHTR_MAX_MOLECULES_COUNT);
    return RES_BAD_ARG;
  }

  if(molecule->cutoff <= 0) {
    log_err(shtr, "%s: molecule %d: invalid cutoff %g.\n",
      caller, molecule->id, molecule->cutoff);
    return RES_BAD_ARG;
  }

  FOR_EACH(i, 0, molecule->nisotopes) {
    if(molecule->isotope_ids_local[i] >= SHTR_MAX_ISOTOPES_COUNT) {
      log_err(shtr,
        "%s: molecule %d: isotope %d: invalid isotope local identifier. "
        "It must be less than %d.\n",
        caller,
        molecule->id,
        molecule->isotope_ids_local[i],
        SHTR_MAX_ISOTOPES_COUNT);
      return RES_BAD_ARG;
    }
  }
  return RES_OK;
}


static res_T
check_shtr_line_view_create_args
  (struct shtr* shtr,
   const char* caller,
   const struct shtr_line_view_create_args* args)
{
  size_t i;
  ASSERT(caller);

  if(!args) return RES_BAD_ARG;

  if(args->wavenumber_range[0] > args->wavenumber_range[1]) {
    log_err(shtr, "%s: invalid line view spectral range [%g, %g].\n",
      caller, args->wavenumber_range[0], args->wavenumber_range[1]);
    return RES_BAD_ARG;
  }

  if(args->pressure < 0) {
    log_err(shtr, "%s: invalid pressure %g.\n", caller, args->pressure);
    return RES_BAD_ARG;
  }

  FOR_EACH(i, 0, args->nmolecules) {
    const res_T res = check_shtr_isotope_selection
      (shtr, caller, &args->molecules[i]);
    if(res != RES_OK) return res;

  }
  return RES_OK;
}

static res_T
create_line_view(struct shtr* shtr, struct shtr_line_view** out_view)
{
  struct shtr_line_view* view = NULL;
  res_T res = RES_OK;
  ASSERT(shtr && out_view);

  view = MEM_CALLOC(shtr->allocator, 1, sizeof(*view));
  if(!view) {
    log_err(shtr, "Could not allocate the line view.\n");
    res = RES_MEM_ERR;
    goto error;
  }
  ref_init(&view->ref);
  darray_size_t_init(shtr->allocator, &view->line_ids);

exit:
  *out_view = view;
  return res;
error:
  if(view) {
    SHTR(line_view_ref_put(view));
    view = NULL;
  }
  goto exit;
}

static res_T
select_lines
  (struct shtr_line_view* view,
   const char* caller,
   const struct shtr_line_view_create_args* args)
{
  const struct shtr_line* lines;
  struct molecule_selection selection[SHTR_MAX_MOLECULES_COUNT];
  size_t imol;
  size_t iiso;
  size_t iline;
  size_t nlines;
  res_T res = RES_OK;
  ASSERT(view && caller && args);

  /* Nothing to do */
  if(args->nmolecules == 0) goto exit;

  /* Setup the selection lookup table that map the isotope of a molecule to a
   * boolean defining if it is selected */
  memset(selection, 0, sizeof(selection));
  FOR_EACH(imol, 0, args->nmolecules) {
    const int32_t mol_id = args->molecules[imol].id;
    ASSERT(mol_id < SHTR_MAX_MOLECULES_COUNT);
    selection[mol_id].cutoff = args->molecules[imol].cutoff;

    if(args->molecules[imol].nisotopes == 0) { /* Select all isotopes */
      FOR_EACH(iiso, 0, SHTR_MAX_ISOTOPES_COUNT) {
        selection[mol_id].isotopes[iiso] = 1;
      }

    } else {
      FOR_EACH(iiso, 0, args->molecules[imol].nisotopes) {
        const int32_t iso_id = args->molecules[imol].isotope_ids_local[iiso];
        ASSERT(iso_id < SHTR_MAX_ISOTOPES_COUNT);
        selection[mol_id].isotopes[iso_id] = 1;
      }
    }
  }

  lines = darray_line_cdata_get(&view->list->lines);
  nlines = darray_line_size_get(&view->list->lines);

  /* Iterate through list of lines to find the ones to select based on spectral
   * range and isotope selection */
  FOR_EACH(iline, 0, nlines) {
    const struct shtr_line* line = lines + iline;
    double nu = 0;

    /* The line is not selected */
    if(selection[line->molecule_id].isotopes[line->isotope_id_local] == 0) {
      continue;
    }

    /* Compute the line center for the submitted pressure */
    nu = line->wavenumber + line->delta_air * args->pressure;

    /* The line is out of the spectral range */
    if(nu + selection[line->molecule_id].cutoff < args->wavenumber_range[0]
    || nu - selection[line->molecule_id].cutoff > args->wavenumber_range[1]) {
      continue;
    }

    res = darray_size_t_push_back(&view->line_ids, &iline);
    if(res != RES_OK) {
      log_err(view->list->shtr,
        "%s: could not register the line into the view -- %s.\n",
        caller, res_to_cstr(res));
      goto error;
    }
  }

exit:
  return res;
error:
  darray_size_t_clear(&view->line_ids);
  goto exit;
}

static void
release_line_view(ref_T* ref)
{
  struct shtr_line_view* view = CONTAINER_OF(ref, struct shtr_line_view, ref);
  struct shtr_line_list* list = view->list;
  ASSERT(ref);
  darray_size_t_release(&view->line_ids);
  MEM_RM(list->shtr->allocator, view);
  SHTR(line_list_ref_put(list));
}

/*******************************************************************************
 * Exported functions
 ******************************************************************************/
res_T
shtr_line_view_create
  (struct shtr_line_list* list,
   const struct shtr_line_view_create_args* args,
   struct shtr_line_view** out_view)
{
  struct shtr_line_view* view = NULL;
  res_T res = RES_OK;

  if(!list || !out_view) { res = RES_BAD_ARG; goto error; }

  res = check_shtr_line_view_create_args(list->shtr, FUNC_NAME, args);
  if(res != RES_OK) goto error;

  res = create_line_view(list->shtr, &view);
  if(res != RES_OK) goto error;
  SHTR(line_list_ref_get(list));
  view->list = list;

  res = select_lines(view, FUNC_NAME, args);
  if(res != RES_OK) goto error;

exit:
  if(out_view) *out_view = view;
  return res;
error:
  if(view) { SHTR(line_view_ref_put(view)); view = NULL; }
  goto exit;
}

res_T
shtr_line_view_create_from_stream
  (struct shtr* shtr,
   FILE* stream,
   struct shtr_line_view** out_view)
{
  struct shtr_line_view* view = NULL;
  size_t nids;
  int version;
  res_T res = RES_OK;

  if(!shtr || !stream || !out_view) { res = RES_BAD_ARG; goto error; }

  res = create_line_view(shtr, &view);
  if(res != RES_OK) goto error;

  #define READ(Var, Nb) {                                                      \
    if(fread((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                   \
      if(feof(stream)) {                                                       \
        res = RES_BAD_ARG;                                                     \
      } else if(ferror(stream)) {                                              \
        res = RES_IO_ERR;                                                      \
      } else {                                                                 \
        res = RES_UNKNOWN_ERR;                                                 \
      }                                                                        \
      log_err(shtr, "%s: error reading isotope metadata -- %s.\n",             \
        FUNC_NAME, res_to_cstr(res));                                          \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  READ(&version, 1);
  if(version != SHTR_LINE_VIEW_VERSION) {
    log_err(shtr,
      "%s: unexpected line view version %d. "
      "Expecting a line view in version %d.\n",
      FUNC_NAME, version, SHTR_LINE_VIEW_VERSION);
    res = RES_BAD_ARG;
    goto error;
  }

  res = shtr_line_list_create_from_stream(shtr, stream, &view->list);
  if(res != RES_OK) goto error;

  READ(&nids, 1);
  res = darray_size_t_resize(&view->line_ids, nids);
  if(res != RES_OK) {
    log_err(shtr, "%s: error allocating the line view -- %s.\n",
      FUNC_NAME, res_to_cstr(res));
    goto error;
  }

  READ(darray_size_t_data_get(&view->line_ids), nids);
  #undef READ

exit:
  if(out_view) *out_view = view;
  return res;
error:
  if(view) {
    SHTR(line_view_ref_put(view));
    view = NULL;
  }
  goto exit;
}

res_T
shtr_line_view_ref_get(struct shtr_line_view* view)
{
  if(!view) return RES_BAD_ARG;
  ref_get(&view->ref);
  return RES_OK;
}

res_T
shtr_line_view_ref_put(struct shtr_line_view* view)
{
  if(!view) return RES_BAD_ARG;
  ref_put(&view->ref, release_line_view);
  return RES_OK;
}

res_T
shtr_line_view_get_size(const struct shtr_line_view* view, size_t* sz)
{
  if(!view || !sz) return RES_BAD_ARG;
  *sz = darray_size_t_size_get(&view->line_ids);
  return RES_OK;
}

res_T
shtr_line_view_get_line
  (const struct shtr_line_view* view,
   const size_t iline,
   const struct shtr_line** line)
{
  size_t i;
  if(!view || !line || iline >= darray_size_t_size_get(&view->line_ids)) {
    return RES_BAD_ARG;
  }
  i = darray_size_t_cdata_get(&view->line_ids)[iline];
  *line = darray_line_cdata_get(&view->list->lines) + i;
  return RES_OK;
}

res_T
shtr_line_view_write
  (const struct shtr_line_view* view,
   FILE* stream)
{
  size_t nids = 0;
  res_T res = RES_OK;

  if(!view || !stream) {
    res = RES_BAD_ARG;
    goto error;
  }

  #define WRITE(Var, Nb) {                                                     \
    if(fwrite((Var), sizeof(*(Var)), (Nb), stream) != (Nb)) {                  \
      log_err(view->list->shtr, "%s: error writing line view.\n", FUNC_NAME);  \
      res = RES_IO_ERR;                                                        \
      goto error;                                                              \
    }                                                                          \
  } (void)0
  WRITE(&SHTR_LINE_VIEW_VERSION, 1);

  res = shtr_line_list_write(view->list, stream);
  if(res != RES_OK) goto error;

  nids = darray_size_t_size_get(&view->line_ids);
  WRITE(&nids, 1);
  WRITE(darray_size_t_cdata_get(&view->line_ids), nids);
  #undef WRITE

exit:
  return res;
error:
  goto exit;
}
